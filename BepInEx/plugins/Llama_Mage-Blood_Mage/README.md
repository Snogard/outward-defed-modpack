# Blood Mage v1.0.1

Utilize the power of your own life essence to heal yourself and damage your enemies.

New Status:
Hemorrhage: A burst of %hp damage over a brief period, removes any bleeds upon activating.  

## From Trainer:
A shady mage has appeared in Harmattan. It's said he can be found skulking in a corner
near the closed gate by the Speedster trainer. 

#### Harness Blood
Inflict bleeding upon oneself. Allows for casting of blood magic.
If bleeding, use to further increase your bleed level. 

## Skill Tree

### Universal Skills
Different levels of bleed effect the power of these spells.

#### Vile Word: Manipulate
While bleeding, redirects your life forces to your health.
Removes stamina, increases burnt stamina, makes the player more tired.

#### Vile Word: Harm
While bleeding, fire a projectile that deals decay damage and inflicts bleeding.

#### Vile Word: Exchange
Fires a projectile that deals damage on a bleeding enemy, consuming the bleed
and healing the caster. 

### Breakthrough Skill

#### Vile Pact
Passive
You regenerate 0.1% of max hp per second. 

### Specialization Skills

#### Hypovolemia
Passive
You take less (50%) damage from bleeding but become thirstier (100%) faster.

#### Leyline Abandonment
Passive, Paired
You sever your ties with the leyline, converting all mana back into health and stamina. 
All spell costs now apply to health instead of mana. No bonuses from mana reduction is gained.

#### Leyline Entanglement
Passive, Paired
You've compromised on your power sources. All spells now split their costs between 
health and mana. Mana cost reduction applies first, then split.

#### Plebotomist
Passive
While bleeding, certain skills gain additional effects. Usage of these skills causes
hemorrhage build up.

### Phlebotomist Combos
These cause varying levels of hemorrhage to build up on the caster.
Like the Word spells, bleed level effect the power of these skills.

#### Spark
Blood Boil -> Create a small blood mist, dealing fire/decay damage.

#### Mana Ward
Humours Maintanence -> Gain resistance to decay and ethereal damage.

#### Conjure
Blood Tide -> Apply decay damage in an area around you, or if extreme bleeding, cause hemorrhage on enemies.

## Changelog
v0.9.1 
- Attempt to fix Leyline Abandonment, was unsuccessful.

v0.9.2
- Leyline Abandonment has been fixed for all normal, non-cheated scenarios
- Vile Pact will now correctly apply its regeneration effect. In theory, no action by players will be needed, should fix itself on load.
- Two of the status effects I made for Humours Maintanence were mutually exclusive with their clone, fixed this.

v0.9.3
- Leyline Entanglement had a thing broken in the logic for allowing casting. 
- Added descriptions to the Word skills
- Fixed the README a little

v0.9.4
- Leyline Entanglement was more broke than I thought

v1.0.0
- Implemented a fix for when the Word projectiles don't hit something, thank you Faeryn!
- I'm willing to take the L on Shady Mage's tree locking, since it is such a non-issue.

v1.01
- Ya boy straight up didn't put the plugin in the last release and the markup was borked.

## Development Plan
===v1.0===
- Fix any bugs, address incompatibilities.

There has been a minor change in what I'd like to do for the mod. For those of you who
enjoy this in the current iteration, 1.0.0 will always exist as it is. However, upon reflection of the 
skill tree I realized it got a little contaminated with my more unique ideas so I'd like to 
morph this skill tree into one more akin to the lore of the game, while making another
skill tree that is in the spirit of stuff I did here. I don't have a sketched out plan yet but I will.

## Additional Comments
Leyline Abandonment will cause you to have 0 mana points. Some other mods use mana points for stuff.

## Feedback
Please direct all feedback, bugs, incompatibilities, etc to Breadcrumb_Lord in the
Blood Mage channel of the Outward Modding Discord. My goal is to keep compatibility with every thing that exists and to
promote synergy between my tree, existing trees, and future trees. 

## Credits

Thank you to Mefino and Sinai for the mod template and usage instructions. Further thanks to everyone in the discord who has
helped me so far, especially Emo for the vfx help, and Stormcancer for the artwork! Thank you Faeryn for the projectile bug fix!
