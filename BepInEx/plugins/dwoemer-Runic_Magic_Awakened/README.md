!! Spoiler document added.  Read at your own risk.  !!

This runic magic expansion allows a runic mage to fight as a mage (including the running!).  There are two major improvements.  The first is having elemental attunement allowing the caster to use Light, Ice, Decay, Ethereal and Fire in the spells.  The spells affected are the runic missiles, the runic traps and the runic blades.  

The runic blades now have mana efficiency at a similar level as other equipment; one-handed is +10 and the two-handed is at +20.  

The second major improvement is the summoned bow. The summoned bow is available with a mana efficiency of +20 BUT it also acts as a Lexicon.  This is the great news!!  With this bow, the caster can target enemies with spells as a bow targets enemies in zoom mode.  As a recommendation, using Hunter's Eye, you can lock onto a target at a far greater distance to cast projectile spells.  And adjust the position of the spell on the target like a bow!

There is also summonable mage armor.  The summoned items will appear in your inventory and they only last for 15 minutes (or longer if you log out and come back in, weird).

With this mod, it is a process to become a powerful mage. There is a "treasure" hunt for power and it starts with a trainer in a dungeon near Cierzo.

Included is a spreadsheet which has the rune combinations for the spells in a couple of formats.

I would like to thank Azrieldemortis (Unlimited Rune Works) and NocturnalTrance (Runic Magic Revamped) for their mods which helped me develop this mod.  This mod was written entirely using SideLoader.  Thank you very much sinaidev for this programming interface. 

This mod is not compatible with other mods which update the runic magic system.  I've also tweaked the in-game Scholar armor set, which I cloned for the summoned armor.  I also gave the Lexicon and Light Bender's Lexicon a boost in mana efficiency, +5 to both.

Now, go out and fight like a mage!!

If you would you like to contact me regarding this mod, I can be reached in Discord.  Mention me in a note:  @dwoemer.


The level at which these spells are acquired:
Base level:	Runic Lantern on and off
		Runic protection
		Runic blade 1h (ethereal)
		Runic trap (ethereal)
Arcane Syntax:	Attunements (attune your spells to one of the elements)
		Mana Absorption
		Runic protection heal
		Runic missile
		Summon magical outfit
		Runic trap detonation
		Runic protection detonation (low damage but high knockback for escaping)
		Healing orbs (healing yourself and allies including summoned ghosts)
		Greater Runic blade 2h 
		Summon bow


Change log

1.0.2	Tweaked a few more over-powered items to make the game more challenging.  
	Corrected a problem with Internalized Lexicon.  Thanks to Emo!
	Fixed a bug with Ethereal runic trap at lower levels.
	Moved the Ancient One trainer to a more dangerous location.

1.0.1	Tweaked a few over-powered items.  
	Reduced prices.  
	Reduced distance requirement between runic traps (was 20 is now 10).  
	Fixed a casting bug for Arcane Syntax level spells.  
	Added spoiler document.

1.0.0  Initial release

