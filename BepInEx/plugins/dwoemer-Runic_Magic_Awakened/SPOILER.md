First of all, where are the trainers.
Chersonese		Wilhelm the Wanderer	Blister Burrow		North side of the dungeon with the legacy chest
Enmerkar Forest		Selma the Wise		Royal Manticore's Lair	In the treasure room with the legacy chest
Abrassar		Henry the Hoader	Undercity Passage	Back area of the library; accessed with the torture room levers
Hallowed Marsh		Listal of Monsoon	Giant's Ruins		In the gated area with the troglodytes
Enmerkar Forest		Ancient One		Cabal of Wind Temple	Upper level

Next, the Zed rune combinations:
Zed - Egoth+	Summon Archmage Deadly Blade/Great Deadly Blade - not attuned 
Zed - Fal+	Increase speed by 30 for 30 seconds
Zed - Dez+	Summon Ghost using soul spot
Zed - Shim+	Double attack speed of melee weapon and double stamina cost, ie. -30 becomes -60; this lasts for 15 seconds.
Zed - Zed	Teleport using lock to be just behind the target
Egoth+ - Zed	Archmage spell projectile which are 25% more powerful than Egoth - Shim
Fal+ - Zed	Teleport 100 units directly forward
Dez+ - Zed	Teleport 30 units directly forward
Shim+ - Zed	Archmage runic traps which are 25% more powerful than Shim - Fal

A word of caution using the teleport ahead spells.  The Fal - Zed teleport spell when directed toward an obstruction will sometimes leave you in the Void.  I've never died in the Void but it was weird!

The basic runes will not work with the higher level runes.

If you have acquired the Internalized Lexicon or Runic Prefix, you will be able to become an Archmage.  After the advanced runes have been used and then the Zed rune is used, you will be recognized as an Archmage.  When this occurs, advanced spells will automatically produce Archmage level effects.  You will receive Archmage summoned armor, Archmage summoned bow and Archmage mana absorption instead of the Master level. 

The Zed rune requires extra power to cast spells.  The cost will be from 5 mana, 10 mana and 20 mana depending on the spell. 

I hope you have noticed that you can run and cast runes while using the advanced or Zed runes.  You don't stop to write the rune and then continue running like you do with the normal runes.  Also, the cooldown for the runes is negligible.  

Multiple runic traps can be put down as long as they are 10 units apart and are of different attunements and/or spell levels {ie. normal, master and Archmage; 15 total).

When you combine the Archmage deadly blade with the warrior buff (Zed - Shim), the fight will be over fairly quickly.  The Archmage doesn't mess around when it comes to melee fighting.  And if it isn't working, the Archmage runs away to fight another day!
