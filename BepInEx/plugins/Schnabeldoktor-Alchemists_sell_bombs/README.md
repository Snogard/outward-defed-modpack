## Alchemists sell bombs Mod by Schnabeldoktor

This mod adds the bombs from TTB DLC to different stores throughout Aurai. You will find the bombs, Sulphur, Chalcedony, Bombkits and recipes in different cities.
In order to balance it, you will find that not all cities have all bombs and that ingredients will have limited amounts. The nerve bomb (ethereal) will be the hardest to find, but not impossible!

# The changes are such:
* Alchemists will have bomb recipes, bombs, bombkits, but the Cierzo one will have the least of them.
* All Alchemists will have oil and spike bombs.
* Blacksmiths will have chalcedony 
* The Harmattan Marketplace will be the place for some more advanced bombs
* The Levant Engineer will have special knowledge and stock for other more advanced bombs
* Cooks will sell Boreo Blubber 
* A Hidden Location for Ambraine
* Bombs added to stores by the latest Outward patch are untouched

Other mechanics (damage, weight, price, etc.) are not touched by this mod.
Also - given that Ambraine is needed for nerve bombs, I added a location to buy them outside of Caldera. Hint: given its effects, this rare ware might only be found among more shady sellers *wink wonk*

I added ALL THE CHANGES below if you want to spoil yourself and know the exact location of all items.

This mod should not interfere with any other mods unless, idk, they remove sellers from the game or something like that. If you have some questions about this mod, you can ask me on the Outward modding discord.
However - I warn you - I did this with very robust knowledge of anything programming-related, so don't expect any thorough answers.

BIG THANKS TO: Nine Dots for making Outward, Gep for financing it. Sinai for SL, Emo for SL Extended Effects. StormCancer, IggyTheMad, Avrixel, Breadcrumb_Lord, Proboina and Emo for their endless patience and help. The Outward Modding Community for their support, ideas and amazing atmosphere.

# ROADMAP

* Bombs, Ingredients and recipes are sold by merchants (DONE)
* Recipes are more productive (made a different mod for that, check my profile - DONE)
* Bombs can be looted from stashes, more enemies or even chests (Might be added to the other mod)
* Bombs are used by more enemies in the game (Discarded - SL can't do that yet) 


# FAQ: 

* Q: Why?

A : Because I hate that bombs are such a cool item yet they can ONLY be accessed after completing 90% of the game. Bombs aren't op and should be more accessible. Also I really like Alchemists in Fantasy lore.

* Q: What does this actually do?

A: I added custom droptables to some merchants throughout the game. 

* Q: Are any other mechanics related to bombs changed by this mod?

A: No.


# CHANGELOG:

* `v1.0.0` - release

* `V1.0.1` - Typos, Hotfix and Readme Structure

* `V1.0.2` - New Release, replacing old version.

* `V1.0.3` - Hotfix, because apparently I can't read or count

* `V1.0.4` - update to latest SL extended effects, slightly raised the amount of chalcedony found in blacksmiths. 

* `V1.0.5` - Added thanks 

-----------SPOILERS BELOW---------------------

* Cierzo Alch: Sulphur, Sharpnel, Oil. Recipes (Sharpnel and Oil Bombs).
* Smuggler* in Levan: Sulphur, Sharpnel, Oil, Frost, Toxin, Nerve Bombs.
* Conflux Watcher: Recipe(Nerve)
* Berg Alch: Sulphur, Bombkit, Sharpnel, Oil. Recipes (Sharpnel, Oil, Nerve, Frost, Spark)
* Monsoon Alch: Sulphur, Bombkit, Sharpnel, Oil. Recipes(Sharpnel, Oil, Nerve, Frost, Spark)		
* Levant: Sulphur, Bombkit, Sharpnel, Oil. Recipes(Sharpnel, Oil, Nerve, Frost, Spark)
* Engineer in Levant: Bombkit, Oil, Flaming, Blazing, Recipes (Sharpnel, Oil, Flaming, Blazing)
* Harmattan Hunter: Bombkit, Sharpnel, Frost, Toxin, Spark. Recipes (Sharpnel, Oil, Frost, Spark, Nerve, Toxin)
* Harmattan Alch: Sulphur, Bombkit.
* Ogoi in Berg: Sulphur, Bombkit.
* Agatha in Chersonese: Sulphur, Bombkit.
* Mathias Shop in Berg (Purifier Quest): Toxin. Recipe(Toxin)
* Pholiota**: Bombkit, Oil, Flaming, Frost, Toxin, Blazing, Nerve, Spark.
* Apprentice Ritualist: Bombkit, Oil. Recipes (Sharpnel, Oil)

*(Tamara the Smuggler in the Levant Slums will sell Ambraine and bombs After HK Side Quest "Blood under the Sun" has been finished with success)	

**Pholiota will only sell bombs if he has his high stock.
	
Fat (Hippos): COOK HARMATTAN/BERG/MONSOON 

AMBRAINE: TAMARA SMUGGLER 

CHALCEDONY: BLACKSMITHS
