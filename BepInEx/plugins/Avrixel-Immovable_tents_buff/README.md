Buffs the effect the one time tents provide:<br>

**Plant tent**:<br>
 Added a -5% stamina consumption<br>
 
**Scourge Cocoon**:
 Increased stamina consumption reduction from -5% to -15% (same as fur tent)
 Added 5% decay damage and resistance