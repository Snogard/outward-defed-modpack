Adds new Tents into the game<br>

Spellblade tent: <br>
Luxury tent which gives you 10% stamina and mana reduction<br>
Sold by Goldbelly, Shopkeeper Suul and Shopkeeper Pleel<br>

Invigorating plant tent: <br>
Plant tent that gives 5% stamina reduction and slowly restores burnt stamina<br>
crafted using Plant tent, Bitterspicy tea and mana stone<br>

More tents will be added in the future, if you have any ideas/feedback feel free to post them in my workshop in Otward Modding discord