[Manastone Blade Stats]
(Damage: 25 Ethereal),
(Impact: 20),
(Speed: 1),
(Mana Regen: 0.05),
(Durability: 250),
(Weight: 5),
(Recipe: Iron Sword, Hackmanite, Hackmanite, Crystal Powder)
This weapon can be put into the legacy chest to get an upgraded version.