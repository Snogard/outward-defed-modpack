# CustomAttributes Dependency Mod
______________________________________________________________
This is a dependency used by other mods to process new custom stats.
- **Critical Hit Chance** (chance to deal increased damage on direct hits, doesnt trigger from damage over time)
- **Critical Heal Chance** (chance to deal increased healing, from direct heals and heal over time)
- **Critical Scaling** (increases the effects of critical hits and critical heals) (50% by default)
- **Avoidance** (chance to completely avoid a direct hit, can not trigger from damage over time)
- **Leech** (restore a percentage of damage dealt to your stamina or mana, whichever percent is lower)
- **Self Critical** (chance to be critically hit by enemies instead)

**Note:** CriticalHit, CriticalHeal and Avoidance have bad luck protection for players, slightly increasing the chance of success every failed attempt, to prevent bad luck streaks. Like most games do.

**For user:** Just download if a mod tags it as a dependency (r2modman should download automatically)

**For Modders:** You can all make use of the new custom attributes and make all our mods compatible and work together! 
Any suggestions are also welcome.

______________________________________________________________
### --- HOW TO IMPLEMENT TO YOUR MOD (c# only)---

1. Add the .dll as a reference in your c# project.
2. Remember to add "using CustomAttributes; at the top of your c# class."
3. On your base plugin "Awake" method, we need to tell the game which Passives or StatusEffects grant what CustomAttribute and by how much (detailed in the next section).
4. Optionally, If you need to run custom code when certain event happens (like a critical hit), when can subscribe to events in the "Start" method.
4. Thats it! The mod will calculate the total stats from all mods and do the appropriate calculations and effects.

______________________________________________________________

## --- CustomAttributes Methods() with examples ---
### Store Passives (this is added on Awake() method)
**Passives stored this way will grant whatever percent you add, of the specified attribute/s**
(**"value"** is the Attribute percent you want to add. Ex: 10 would add 10% of that attribute)
**Call the CustomAttributeHelper and run the method you need**
- PassiveCritRateStore(int skillID, float value)
- PassiveCritHealStore(int skillID, float value)
- PassiveCritScalingStore(int skillID, float value)
- PassiveAvoidanceStore(int skillID, float value)
- PassiveLeechStore(int skillID, float value)
- PassiveSelfCritStore(int skillID, float value)

Example: This would make the Passive skill "Fitness" with ID 8205040 grant 10% critical hit chance

 ``` internal void Awake() { CustomAttributeHelper.PassiveCritRateStore(8205040, 10f); } ```
    
### Store StatusEffects (this is added on Awake() method)
**StatusEffects stored this way will TEMPORARILY grant whatever percent you add, of the specified attribute/s**
(it also works with leveled status effects, stacking per status level)
(**"value"** is the Attribute percent you want to add. Ex: 10 would add 10% of that attribute)
**Call the CustomAttributeHelper and run the method you need**
- StatusCritRateStore(string skillID, float value)
- StatusCritHealStore(string skillID, float value)
- StatusCritScalingStore(string skillID, float value)
- StatusAvoidanceStore(string skillID, float value)
- StatusLeechStore(string skillID, float value)
- StatusSelfCritStore(string skillID, float value)

Example: This would make the StatusEffect "Rage" grant 5% avoid chance

 ``` internal void Awake() { CustomAttributeHelper.StatusAvoidanceStore("Rage", 5f); } ``` 
    
### Running Custom code after events (this is added on Start() method)
You can trigger special code when a player deals a critical hit, critical heal, if they avoid a hit or if critically hit by an npc.
**Call a CustomAttributesBase instance and subscribe to the event you need**
- OnCharacterCritEvent += YOUR CUSTOM METHOD HERE
- OnCharacterCritHealEvent += YOUR CUSTOM METHOD HERE
- OnCharacterAvoidEvent += YOUR CUSTOM METHOD HERE
- OnCharacterSelfCritFromAI += YOUR CUSTOM METHOD HERE

Example: This would run a custom method on your mod whenever a player critically heals succesfully

 ``` internal void Start() { CustomAttributesBase.Instance.OnCharacterCritHealEvent += PlayerGainsGoodStatusEffectOrSomething; } ```
    
### Additional Methods
You can also get the total accumulated attributes from players if you need them for anything.
By getting a reference to their CustomAttributeComponent from the Character itself.

Ex: ``` CustomAttributeComponent playerCustomAtts = _character.GetComponent<CustomAttributeComponent>(); ```

- CalculateFinalCritRate()
- CalculateFinalCritHealRate()
- CalculateFinalScalingCrit()
- CalculateFinalAvoidance()
- CalculateFinalLeech()
- CalculateFinalSelfCrit()

All other events are public in case you need them, but the ones presented here should be enough for any mod.

______________________________________________________________
### Contact: Discord
Any questions, bugreports, feedback or just to say hi, join the Outward Modding Community discord and leave it at the iggythemad-workshop channel, or feel free to message me directly #iggythemad .

______________________________________________________________
##### CHANGELOG:

- v1.1.0 Added f5 options for notifications and flying text

- v1.0.1 Fixed crit and avoid triggering from damage over time

- v1.0 Release