## Multiple Quickslot Bars

Allows you to add extra quickslot bars which you can cycle through with custom keybinds.

Set the custom keybinds in the in-game keybinding menu, and the amount of extra bars can be set with the [Config Manager](https://outward.thunderstore.io/package/Mefino/Outward_Config_Manager/), or the r2modman Config Editor, or by editing the file at `BepInEx\config\com.sinai.multiplequickslotbars.cfg`.

This mod does not add extra slots, but instead swaps out the entire bar for alternate bars. You can configure the mod to add as many extra bars as you want, but currently you can only cycle between them one at a time (no direct hotkeys to jump to a specific bar, etc).

### Changelog

* `2.0.0` - changed how quickslots are saved. This is a ***breaking change*** for the save data, you will need to set up your bindings again sorry. But this fixes a couple of bugs.