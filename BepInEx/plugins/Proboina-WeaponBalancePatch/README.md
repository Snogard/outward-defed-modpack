# Unnofficial Weapon Balance Patch

## Overview
That mod is aimed at giving some love to some weapons you almost certanly skip during your general playthrough by giving them a special effect, changing their behavior, providing more enchantability or just changing some stats.

## Installation
- 1 Download [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)
- 2 Download [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)
- 3 Unpack the contents of the zip file into the ...OutwardDefinitiveEd\BepInEx\plugins\WBP

### You may install modules of that mode seperately if you want to have specific changes, but don't want to have all of them. Isntall either the whole package from that page, or seperately. Do not install both!

#### If you want to read more about changes I made, each link has my thought-process behind the change. If you disagree with any of them - you're free to write me, I am interested in your opinions. 

## List of Changes:

### [ARTIFACT CHANGES - Main Module](https://outward.thunderstore.io/package/Proboina/Artifacts_Reworked/)

- **Assasin Sword and Claymore**: have 1.1 speed and apply "Crippled" instead of "Slow Down"

- **Butcher Cleaver**: 20 Physical 20 Ethereal, gives "Craze" on kill

- **Cerulean Saber**: has 1.1 speed

- **Crescent Scythe and Axe**: can be enchanted with "Spectre's Wail" with higher blast damage

- **Dragon Shield**: new Legacy Item: Awakened Dragon Shield. Better stats, 7% fire damage bonus and breathes fire on successful charge

- **Fabulous Palladium Shield**: Inflicts all elemental Hexes instead of "Elemental Vulnerability". Praise me, hex mages

- **Gep's Blade/Longblade**: Changed the recipe, now uses only one Gep's Drink, Digested Mana Stones and Ectoplasm

- **Giantkind axe**: can be enchanted with "War Memento"

- **Giantkind halberd**: does 25 Physical 10 Lightning, can be enchanted with "Priest's Prayers"

- **Gold-Lich Weapons (except Shield)**: can be enchanted with "Flux"

- **Golem Rapier**: has 1.2 speed and a bit lower impact

- **Jade Lich Scimitar**: 16 Physical 16 Decay, no mana cost reduction, corrupts the wielder with every swing (around 3 hits for 1%)

- **Jade-Lich Mace**: has 7% mana cost reduction and applies another 20% for 90 seconds after a few hits.

- **Livingwood axe**:  16 Physical 6 Ethereal, inflicts "Aetherbomb" with low buildup

- **Manticore Greatmace and Dagger**: can be enchanted with "Instinct"

- **Mantis Greatpick**: can be enchanted with "Weightless"

- **Mysterious (pizza) Chakram**: can be enchanted with "Abundance". Actively regen your food with pot helmet, don't let anyone in slums starve!

- **Old Legion Gladius**:  has a 15% chance to critical strike for 100 Raw

- **Ornate Bone Shield**: inflicts "Elemental Vulnerability" and "Haunted"

- **Palladium Knuckles**: no longer apply "Elemental Vulnerability"

- **Pathfinder Claymore**:  Physical and Decay damage values swapped, 1.0 speed, does not apply "Elemental Vulnerablity". Inflicts "Status Build up Resistance Down" both on player and enemy, inflicts "Curse" and "Extreme Poison"

- **Phytosaur Spear**: 34 Physical damage, can be enchanted with "Abomination"

- **Porcelain fists**: apply "Calcification" status effect that stuns enemies for a couple of seconds.

- **Pyrite Greathammer**: can be enchanted with "Forge Fire"

- **Royal Great-Khopesh**: Inflicts "Weaken", "Pain" on enemy and "Stamina cost reduction" build-up on player. Can be enchanted with "Desert's Sun"

- **Sandrose Axe**: also inflicts "Scorched"

- **Starchild Claymore**: applies "Divine Light Imbue" with small buildup, lowered impact from 52 to 45. No more cheesy strategies with Wind imbue

- **Thorn Chakram**: small lifesteal

- **Thorny Claymore and Spear**: Different recipe, 22 Physical 22 Ethereal, inflicts "Elemental Vulnerability" if the target is afflicted by "Curse"

- **Thrice-Wrought Halberd**: deals 13 Fire, 13 Frost and 13 Ethereal damage

- **Werlig Spear**: deals only 30 Physical, but has a Chain Lightning effect, dealing 14 Lightning damage to maximum of 4 targets nearby

- **Withering Trog Mace**: has 200 more durability and 1.0 speed

- **Worldedge Greataxe**: applies "Dark Imbue" with small buildup. Restored to it's former glory

- **Zagis' Saw**: also inflicts "Extreme Bleeding", 0.9 speed instead of 0.8


### SETS

- **[Astral set](https://outward.thunderstore.io/package/Proboina/Astral_Set/)**: can be used as a Lexicon
- **[Beast golem set](https://outward.thunderstore.io/package/Proboina/Beast_Golem_Set/)**: does not inflict "Bleeding" anymore, but has 1.2 base speed, has a new enchantment
- **[Chalcedony set](https://outward.thunderstore.io/package/Proboina/Chalcedony_Set/)**: deals Raw damage instead of Physical, has some frost damage bonus, inflicts "Hampered" instead of "Slow Down", can be enchanted with "Snow". Truly an end-game set now.
- **[Forged Glass set](https://outward.thunderstore.io/package/Proboina/Forged_Glass_and_Meteoric_Sets/)**: a couple of new enchantments.
- **[Galvanic set](https://outward.thunderstore.io/package/Proboina/Galvanic_Set/)**: half the damage converted to Frost damage, inflicts "Chill" and "Pain". Chakram can be enchanted with "Wendigo's Breath"
- **[Hailfrost set](https://outward.thunderstore.io/package/Proboina/Hailfrost_Set/)**: mostly Frost damage, but a bit lower damage overall.
- **[Horror set](https://outward.thunderstore.io/package/Proboina/Horror_Set/)**: can be enchanted with "Scourge's Outbreak", 0.9 speed and lowered sell price (It was too high and you know it)
- **[Kazite set](https://outward.thunderstore.io/package/Proboina/Kazite_Set/)**: applies "Unerring Read" on wielder with 10% chance, maces give 13%, knuckles and chakram 7%, shield and pistol 23%
- **[Marble set](https://outward.thunderstore.io/package/Proboina/Marble_Set/)**: all weapons now inflict "Confusion"
- **[Meteoric set](https://outward.thunderstore.io/package/Proboina/Forged_Glass_and_Meteoric_Sets/)**: inflicts "Blaze" instead of "Holy Blaze", like a proper fire weapon upgrade should. Adjusted Inferno enchantment a little.
- **[Smoke set](https://outward.thunderstore.io/package/Proboina/Smoke_Set/)**: inflicts "Weaken" and "Sapped" instead of "Blaze", makes enemies look engulfed in smoke. Cool!
- **[Vampiric set](https://outward.thunderstore.io/package/Proboina/Vampiric_Set/)**: has 1.0 speed and are 7 TIMES faster to acquire. 2500 damage instead of 17500. Leech was slightly lowered as a tradoff, except on maces and the dagger
- **[Virgin set](https://outward.thunderstore.io/package/Proboina/Virgin_Set/)**: has 1.1 speed
- **[Wolf set](https://outward.thunderstore.io/package/Proboina/Wolf_Set/)**: no longer inflicts "Cripple", can be enchanted with "The Good Moon"


### [STAVES](https://outward.thunderstore.io/package/Proboina/Staves_Reworked/)

- **Master's Staff** and **Ivory Master's Staff**: have base 5% Cooldown Reduction
- **Calygrey Staff** and **Ancient Calygrey Staff**: are 2-Handed Maces with appropriate stats, but keeping the magic properties. Great for "Crescendo"!
- **Compasswood Staff**: does 15 Fire 15 Physical, has 10% Cooldown Reduction and gives 10% movement speed bonus
- **Rotwood Staff**:  has 10% Cooldown Reduction, gives 5% movement speed bonus and 0.1 mana per second
- **Jade-Lich Staff**: does 15 Decay 15 Physical, 5% Cooldown Reduction, 3 Barrier and gives Corruption to player on hit
- **Cracked Red Moon**: does pure Fire damage and has 25% Fire damage bonus
- **Revenant Moon**: is OP (25 Ethereal damage, 25% Ethereal and Fire damage bonus and can be used as a lexicon, no longer inflicts "Blaze")

### [ARTIFICER](https://outward.thunderstore.io/package/Proboina/Caldera_Artificer/)

- There has been a secret Trainer placed somewhere. Look at the picture down below to give yourself a hint.

### [ARROWS](https://outward.thunderstore.io/package/Proboina/Arrow_Recipes_Reworked/)

- **Arrowhead Kit (x3)**: can be crafted with 3 "Iron Spikes" and "Palladium Scrap" in survival menu
- **Poison Arrow (x3)**: can be also crafted with 3 "Arrows" and a "Poison Rag" in survival menu
- **Flaming Arrow (x3)**: can be also crafted with 3 "Arrows" and a "Fire Rag" in survival menu
- **Forged Arrow (x3)**: can be crafted with "Iron Spike" and" Arrowhead Kit" in survival menu
- **Palladium Arrow**: crafted in survival menu instead of alchemy, no changes to the recipe
- **Iron Wristbands** can be crafted with 3 "Iron Scraps and "Linen Cloth"
- **Palladium Wristbands** can be crafted with 3 "Palladium Scraps" and "Scaled Leather"
- **Iron Knuckles** recipe changed to have "Iron Spikes" instead of "Boozu Hide"
- No alchemy station arrow recipes were changed besides Palladium Arrow

### New enchantments and changes

- **Brittlesharp** - Glass set - 2x the damage, -90% durability

- **Calygrey's Valor** - Trog Pole Mace, Withering Pole Mace, Calygrey Mace, Ancient Calygrey Mace - +0.1 speed and 20 "Rage" buildup on player

- **Castigation and Humiliation** - Prayer and Sinner Claymores, Ornate Chakram - also applies "Barrier +6" with Castigation and "Protection +6" with Humiliations on player respectively, no longer enchanted on Palladium set

- **Castle Wall** - Tower Shield and Marble Shield - "Impact Resistance Up" buildup on player on block

- **Catharsis** - Zhorn's Demon Shield - applies "Panic" on player and gives "Blood Imbue" for 15 seconds

- **Desert's Sun** - can be enchanted on Khopesh type weapons

- **Golem's Effectiveness** - Golem Rapier, Beast Golem weapons - +0.1 speed and "Pain" buildup, making those 3 the fastest weapons in the game

- **Tsar Inlay** - Ecnhanting Guild, requires "Tsar stone" - Any weapon - indestructible, lowers the attack speed by 0.1.

- **Inferno** - no longer inflicts "Blaze", Converts 80% of Physical damage to Fire, removes 50% of Physical damage.

- **Innate Fury** - Crimson Shield - "Rage" buildup on player on block

- **Jade Mirror** - Jade-Lich Staff - 100% status resistance to all poisons (including marsh water) and "Plague"

- **Labouring Perfection** - Enchanting Guild, requires "Chromium Shards" - Golden Pick and Golden Harpoon - unbreaking and -4 weight

- **Mantle** - Wolf Shield - "Force Bubble" on successful shield charge

- **Monk Training** - Quarterstaff and Gold Quearterstaff - sets damage to 45 Physical and +5% movement speed. Enchanting Guild recipe, requires "Diamond Dust".

- **Overcharge** - Gold-Lich Shield - Lightning explosion on blocked hits, but causes "Drawback" buildup. No explosions while "Drawback" is active.

- **Razor of Woe** - Rondel and Shiv daggerss - adds a 2% chance to instantly kill a target. Enchanting Guild recipe, requires "Dweller's Brain".

- **Redemption** - Palladium set - also gives 5 flat Lightning damage, no longer inflicts Doomed and can't be enchanted on Prayer or Sinner Claymores

- **Righteous** - Glass set - Holy Blaze buildup

- **Roaring Sun** - Palladium Shield - "Immolation" on successful shield charge

- **Sharpened Edge** - any Chakram - adds 7 Physical damage

- **Sprectre's Wail** - Crescent Set - higher blast damage

- **Trauma** - rebranded as a Forge Fire enchantment. Removes just 10 physical instead of 27. Uses the Forge Fire recipe, no longer an Enchanting Guild recipe.

- **The Good Moon** - Wolf set - does 15% mana steal instead of 20%, no longer enchanted on Rondel and Shiv daggers

- **Weightless** - can be enchanted on "Mantis Granite Pickaxe" with higher weight reduction

- **Whiplash** - adds 20% Imapact instead of 10% Physical damage, Maces and Knuckles get 10% impact bonus.

#

All new enchanting recipes are sold by Hermit and Harmattan vendors. All changes in existing enchants are reflected in existing recipes.  
P.s. Every change with "also" means that the new effect is added on top of existing one

#### Known bugs: 
Incompatibilty with ActionUI's enchanting menu - I'm looking into the problem, but for the time being you'd have to resort to old reliable enchanting table

Some enemies may spawn with old weapon stats, kazite bandits in Harmattan, for example. Reload fixes that problem.

You Skilltrainer may be not updated or refuse to open the skilltree - to fix the problem try waiting 7 days for area reset, if it doesn't help - clear BepInEx cache in BepInEx/cache.

## NO BOWS WERE CHANGED;

- Report to me any balance changes you feel need to be done on the [Outward Modding Community](https://discord.gg/zKyfGmy7TR) Discord
I'd love to hear if you have any other ideas for improvements. Proboina-Workshop channel.

## Special thanks
- **Emo** for teaching me and helping along the way
- **Iggy** for giving me confidence in my project
- **Avrixel** for a bunch of ideas and help implementing them
- **StormCancer** for beautiful skill icons
- Every streamer I pestered
- And everyone in the awesome discord modding community

Have fun!

![Chain Lightning](https://i.imgur.com/2sSJ87K.gif)
![Artificer](https://i.imgur.com/ApwMdCN.png)
![Awakened Dragon Shield](https://i.imgur.com/q7PRNn8.png)

## Changelog:

- `1.0.0`: initial release
- `1.0.4`: added r2modman compatibility
- `1.0.6`: addeed palladium to the arrowhead recipe
- `1.1.0`: Finally Modular! Plus a lot of changes and corections, 3 new sets and a little bit more on top. All the changes you can see in Discord channel.
- `1.1.3`: Added Werlig Spear with Chain Lightning Effect
- `1.1.5`: Returned half of the Ethereal damage to Livingwood Axe; Porcelain Fists stun for longer; Fixed Forged Glass and Werling spear not having new effects.
- `1.2.1`: Cool~ update - Added Galvanic and Hailfrost sets; Fixed Redemtion working improperly; Razor of Woe enchanting item changed
- `1.3.0`: Holy Shield! update - changes to Palladium and Sinner/Prayer enchantability, a whole bunch of new shield enchants and changes and some minor weapon balance
- `1.4.0`: Final update. Maximum possible compatibility, a lot of tweaks and changes, a whole new secret skilltree and much more! Thanks everyone who has been helping me create the mod to be the way it is right now, I couldn't have done it without you
- `1.5.0`: Final Final update.. Fixed Pestilence; Added missing Artifact Skills; Calcification gives impact immunity for more consistent stun; Butcher Cleaver gives 3 stacks of Craze; Added Sharpened Edge enchanting recipe to vendors;
- `1.5.1`: Raised Kazite set dodge chance; Lowered Werlig Spear's projectile from 16 to 12
- `1.6.0`: A LOT of polish, some new enchantments, a couple more reworks of simple weapons. Removed Brand changes for better rework, prepared ground for more mod compatibilities