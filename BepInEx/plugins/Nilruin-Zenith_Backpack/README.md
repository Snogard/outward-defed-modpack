## Zenith Backpack

This mod not only adds a handful of new backpacks to the game, it also adds a recipe for the Chalcedony Backpack (I didn't agree with it being locked behind New Sirocco's restoration, considering you have to clear the main storyline before you can even start restoring New Sirocco) and I also added the Preservation Backpack to Balira's drop table, (many players clear Vendavel before they even start the main questline, I know I did).

I tried to incorporate these new backpacks using a combination of high-end backpacks acting as intermediary ingredients. I also tried to keep the stats in line and not make the new backpacks too overpowered (except for one, which was on purpose).


### New Recipes

- Chalcedony Backpack: any backpack, one nepherite gemstone, and two chalcedony.

- Alpha Backpack: Zhorn's Hunting Backpack, Boozu Hide Backpack, Brass Wolf Backpack, and the Brigand's Backpack.

- Omega Backpack: Mefino's Trader Backpack, Weaver's Backpack, Dusk Backpack, and the Preservation Backpack.

- Zenith Backpack: Alpha Backpack, Omega Backpack, Chalcedony Backpack, and a Tsar Stone.


## Notes

I tested killing Balira a few times to make sure the backpack was a guaranteed drop. This is my first time adding to a drop table, please let me know if anyone finds anything wrong. I killed a few ice witches to make sure it didn't get added to their drop table, just in case. I can't imagine it would be, considering that Balira actually has her own drop table, DT_VendavelWitch, as opposed to the normal Ice Witch drop table, which is just DT_IceWitch.

The Zenith Backpack can be upgraded further. There's a reason why the stats on the Zenith Backpack are slightly less than the combined stats of what was used to create it. The upgraded form has the full stat block. The description of the backpack should make it obvious what you need to do in order to upgrade the backpack, but in case anyone wants another hint: it's not upgraded via crafting.



## Latest changes

* `2.0.1` - fixed spelling errors in the description of the mod

* `2.0.0` - completely overhauled the entire mod, added alpha and omega backpacks, converted zenith recipe to include the two new intermediary backpacks and a chalcedony backpack and a tsar stone, added an upgrade to the zenith backpack with the full stat block of all included backpacks, balanced the stats on each of the new and old modded backpacks to add a sense of progression to the entire mod, added a chalcedony backpack recipe, added the preservation backpack to Balira's drop table

* `1.1.0` - added -10% stamina cost to zenith backpack, inherited from Zhorn's Hunting Backpack. Also lowered capacity from 250 to 200 and lowered corruption resistance from 20 to 10 because the Boozu backpack only grants 10 res and not 20 res. This will make the backpack not feel so overpowered in my opinion.

* `1.0.0` - added zenith backpack to the game

