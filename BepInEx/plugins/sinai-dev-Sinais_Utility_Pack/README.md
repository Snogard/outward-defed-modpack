## Sinai's Utility Pack

Some small utility / quality-of-life mods which don't warrant separate releases.

* **NOTE**: This mod requires [Outward Config Manager](https://outward.thunderstore.io/package/Mefino/Outward_Config_Manager/). You must enable each of the plugins that you want to use from the config menu.

Features:
* Better Targeting
* Building Helper
* Custom Time Speed
* Custom Weight
* Disable Mouse Smoothing
* Dismantler
* Enchantment Helper

## Manual Install
Put `UtilityPack.dll` in `Outward\BepInEx\plugins\`

## Changelog

* `1.2.0` - updated for DE
* `1.1.2` - fixed Better Targeting causing a delay when acquiring a target for the first time
* `1.1.1` - fixed Character Scaler not resetting scale on disable
* `1.1.0` - added Passive Enemies and Character Scaler. Config names have been adjusted.
* `1.0.2` - made Custom Weight update weight properly when loading character from menu
* `1.0.1` - updated readme
* `1.0.0` - initial release

## Features
### Better Targeting

Removes the automatic target switching behaviour from the vanilla game so that you don't randomly switch targets mid-fight.

Adds a new "Toggle Target" keybind which you can press to manually cycle through the available targets.

Setup:
1. Enable Better Targeting in the config menu.
2. Set a keybind for `Toggle Target` in your keybindings if you wish to use it.

### Building Helper

Debug and cheat tool for building in The Three Brothers.

* Removes build times and costs (instant, free building)
* Allows you to destroy buildings.
* Allows you to theoretically place buildings in other scenes, but can be buggy.

Note: There is a potential bug if you place more than 3 specialized buildings before your 3rd Caldera quest. Remove your special buildings if you cannot progress in the quest.

Setup:
1. Enable Building Helper in the config menu.
2. Set a keybind for `Building Helper Menu` in your keybindings.
3. Open the menu while in-game to use it

### Character Scaler

Simple tool to set a custom scale for your player character (currently only affects player 1).

1. Enable Character Scaler in the config menu
2. Set your custom scale size in the "Character Scaler" section. It will probably be buggy if you go further than 0.8 or 1.2, but have fun.

### Custom Time Speed
Allows you to set a modifier on the rate at which time passes in-game.

Setup:
1. Enable Custom Time Speed in the config menu.
2. In the "Custom Time Speed" section of the menu, set your value as desired. This value is a multiplier, so "0.5" would mean time passes at 50% speed, and "2.0" would be 200% speed, etc.

### Custom Weight

A cheat tool for the weight mechanic in the game if you don't want to deal with it.

Setup:
1. Enable Custom Weight in the config menu.
2. In the "Custom Weight" section of the menu, set your desired values for weight penalties, or remove them all-together.

### Disable Mouse Smoothing

Simply disables the "mouse smoothing" effect.

Setup:
1. Just enable it in the config menu.

### Dismantler

Adds Destroy interactions to objects which don't normally have them (Plant Tents, Campfires, etc).

Setup:
1. Just enable it in the config menu.

### Enchantment Helper
A Debug/cheat tool for Enchantments, allowing you to put enchantments on items instantly and without cost.

Setup:
1. Enable Enchantment Helper in the config menu
2. Set a keybind for `Enchantment Helper Menu` in your keybindings
3. Open the menu while a character is loaded and add enchantments to your equipped items

### Passive Enemies

Simply makes all enemies non-aggressive. 