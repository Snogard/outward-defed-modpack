# TrainersOfAurai v5: Chrono-Mage
______________________________________________________________
Original Classes: Chrono-Magic + Spellslinger + Soul-Mage + Mad Whisperer + Apothecaries. 
Plus SelfDefence skill tree with universal perks like **Drink on the move**, Charged heavy attacks, **dodge cancelling**. Also Unique new quests and lore!

Known Incompatibilities or overlaps: 
-Drink While Moving (not necesary, its included in this mod)
-GameplayTweaks: Dodge Cancelling (already included in this mod, and can cause conflicts)
-Classfixes customShoot doesnt work with spellslinger quickdraw. Use vanilla Shoot. Everything else is great.

##### v5.0: Chrono-Mage Update.
______________________________________________________________
### ---Trainers Overview (and where to find them)---

##### --->  **INNs** in **ALL CITIES** have new **GOSSIP NPCs** that will point you toward the  **TRAINER LOCATIONS**. Ask around!

- **(new) Chrono-Magic:** Learn the ways of the lore-walkers time magic. Channel your lexicon (or internal one) to gain utility skills to speed up, paralyze, forward time, rewind mistakes, unleash the time plagues. Fits with every playstyle and can lead to crazy combos.

- **Mad Whisperer:** Master the ethereal void magics and Start a quest that takes you all over aurai to further empower your whisperer abilities. With 14 different upgrade options, you can customize your Whisperer to your playstyle. [Requieres a second breakthrough to unlock upgrades, but trust me: they are worth it]

- **Spellslinger:** Magic and guns, merged into a very fast paced skill tree with teleports, instant reload guns, critical hit chance, and more.

- **Soul-Mage:** Discover how to trap souls, drain mana, and build an unstopable shield of magic. With new options perfect for Battle Mages.

- **Grand Apothecary:** Collect unique recipees to craft powerfull elixirs, tonics and flasks that can be shared with allies. Each city has their own regional recipees.

- **Self Defence:** Small tree for everyone with options like Charging attacks by holding the button, dodge cancelling and drink while moving.
______________________________________________________________

### ---SKILL TREE: CHRONO-MAGIC ---

- **Chrono-Plagues:** All plagues deal electro-frost damage overtime, plus special effects.
--1 **Plague of Time:**  Each damage tick reduces your active cooldowns.
--2 **Plague of Weakness:**  Each damage tick reduces the target resistances.
--3A **Plague of Burnout:**  When the caster deals direct damage, the duration of the plague is reset and the damage dealt by it is increased.
--3B **Plague of Stability:**  Each damage tick also drains stability.

- 4 **[Breakthrough] Timeline Cycle:** Activate to cycle between 3 passives.
-- **Timeline of Haste:** Spending mana increases movement and attack speed briefly.
-- **Timeline of Plagues:** Double the damage and effects of plagues.
-- **Timeline of Survival:** Gain a chance to completely avoid a direct hit and stagger the attacker.

- 5A **Pandemic:** Explosion of time magic that applies all learned plagues to enemies in a radius.
- 5B **Lock Stasis:** Paralyse an enemy briefly. Making them unable to act, but also immune to everything.

- 6A **Fast Forward:** Drain mana every second to increase cooldown recovery by 1000% until canceled or out of mana.
- 6B **Rewind:** Reduce all skill Cooldowns passively.

- 7 **Alter Time:** Store your current self (resources, cooldowns, effects, etc). After a delay or when cast again, return to your stored time.

______________________________________________________________

### ---SKILL TREE: GRAND APOTHECARY---

New apothecaries have arrived to cities, each with unique craftable potions.

- **Craft: Tonic of Borrowed Power:** Allows for mana spells to be cast with stamina instead.

- **Craft: Tonic of the Leech King:** Regenerate hp per second after killing an enemy.

- **Craft: Tonic of Resilience:** Temporarily increases max health.

- **Craft: Flask-A-Pot:** Instant-use flask that restores some resources.

- **Craft: Flask of Timeless Wisdom:** Your next skill used has a 90% shorter cooldown.

- **Craft: Flask of Titans:** Doubles impact dealt by abilities temporarily.

- **Craft: Oil of Scattering:** Weapon-Imbues damage is also spread to all enemies near your target.

- **Craft: Oil of Piercing:** Your physical melee attacks bypass resistances completely, but deal 20% less base damage.

- **Craft: Oil of Bursting:** Successful weapon hits have a chance to grant you greatly increased attack speed briefly.

- **[Passive] Grace of Nature:** Increase movement speed and pouch size temporarily, after gathering edibles.

- **[Passive] Rushed Recovery:** Allows you to drink while moving, but take triple impact while doing so.

- **[Breakthrough] Sharing is caring:** Drinking a potion grants resources over time to user and allies nearby.

- **[Passive] Brewmastery:** Drinking reduces damage taken briefly.

- **[Passive] Master Touch:** When crafting any oil, flask, tonic or potion, theres a chance you will get an extra one for free.

- **[Passive]  Drinker:** Drink apothecary flasks in half the time.

______________________________________________________________

### ---SKILL TREE: MAD WHISPERER---

- 1 **[Passive] Pathway to Insanity:** Generate Insanity every 20s, which passively makes 10% of all direct damage to be taken over 15s instead.

- 2 **Purify:** Reduce currently delayed damage by 40%. Costs 1 Insanity.

- 3 **[Passive] Whispers of Madness:** Dealing damage grants you a stack of Madness (up to 10), which increases damage dealt by 2.5% and damage taken by 5%. Lasts 10s but can be refreshed.

- 4A **[Breakthrough] Pact of Destruction:** Each stack of insanity grants 10% attack speed but reduces impact dealt by 15% and can now stack up to 2. (can only choose one Pact breakthrough)

- 4B **[Breakthrough] Pact of Affliction:** Insanity can stack up to 3 times and its effects are baseline (Not affected by stacks). 
(can only choose one Pact breakthrough)

- 5 **Suppress Pain:** Delay 30% more damage for 5 seconds. Costs 1 Insanity.

- 6 **[Breakthrough] Eye of Servitude:** Serve the whispers to unlock the quest to find upgrade trainers around the world. 
(Unlocks 5 trainers with 2 strong upgrades or new skills each. Plus 1 extra hidden upgrade trainer)
[Unlocks will be kept a secret, but if you wanna know, they will be in the nexus page forum]

______________________________________________________________

### ---SKILL TREE: Spellslinger---

- 1 **[Passive] Lucky Strike:** All attacks now have a chance to critically hit and deal bonus damage! But you are now able to be critically hit by enemies aswell.

- 2 **[Passive] Quick-Draw:** Pistols reload automatically and deal pure magical damage. But cost mana.
(does not work with classfixesMod mainhand pistols yet, I'm still working on a compatibility fix)

- 3 **Spellbound:** Mark a target, which will explode for area debuffs if shot with a pistol.

- 4 **[Breakthrough] Fire Within:** Critical hits dealt grant you a stack of "Inner Fire" which increases critical hit chance further. Stacks up to 3 times.

- 5A **[Passive] Leyline Bullets:** Reduces Quick-Draw mana cost and critical gun shots consume no bullets.

- 5B **[Passive] Bolt Draw:** Starting a normal attack immediately after damaging your target with Quick Draw, teleports you behind the enemy.

- 6 **[Passive] Bolt Rush:** Replace dodge with a faster, short-distance teleport that uses mana instead.

- 7A **[Passive] Clarity:** Dealing a critical hit with 3 stacks of "Inner Fire", grants free mana costs and bolt rushes for 5s.

- 7B **[Passive] Critical Adjustment:** Grants 50% critical scaling, which makes critical hits and heals be more effective.

______________________________________________________________

### ---SKILL TREE: Soul-Mage ---

- 1 **[Passive] Equilibrium:** All stamina costs are split between stamina and mana. In combat only.

- 2 **[Passive] Transfusion:** Increase mana costs, but half of all damage taken is reduced from mana instead, for an extra amount. 
- 3 **Soul-Shatter:** Blast a target for  20 unblockable dmg. Deals 500% extra damage if target is below 30% HP, and half of that as area damage.

- 4 **[Passive] Soul-Keeper:** Soul-Shatter revealed souls to capture them into Soul-Shards. Soul-Shards are required by some spells or can be eaten to grant powerful effects to soul related spells.

- 5A **Drain-Soul:** Tap into an enemy soul to drain mana from them and give it to you, over 20 seconds. Requieres 1 Soul Shard.

- 5B **Soul-Storm:** Cause a soul storm around you, draining mana from every enemy nearby, every second for 10s. Requieres 1 Soul Shard.

- 6 **Soul-Shape:** Become unstoppable! Increasing damage dealt by 0.2% per current mana, and reduce damage received by 0.2% per missing mana. Requieres 2 Soul Shards.

- 7A **[Passive] Soul-Bound:** All damage taken is now dealt to mana instead, for an extra amount. And all health and stamina above 30 auto trades into max mana.

- 7B **[Passive] Soul-Tap:** Dealing physical damage grants mana equal to the damage dealt, but mana is rapidly drained over time. Reveal Soul grants mana out of combat.
______________________________________________________________

### --- Gameplay Tweaks TREE: Self Defence ---

-  **[Passive] Rushed Recovery:** Drink While Moving. Allows you to drink while moving, but take triple impact while doing so. 
(same as apothecary tree)

- **[Passive] Veteran Awareness:** Dodge Cancelling. You can now cancel an attack with a dodge, before hitting an enemy.

- **[Passive] Heavy Hitter:** Charged Attacks. You can now charge weapon attacks to deal increased damage and knockback.

- **[Passive] Fluid Movements:** Increase your attack speed with an empty offhand.

______________________________________________________________

### Contact: Discord (or nexus, but I check them less often)
Any questions, bugreports, feedback or just to say hi, join the Outward Modding Community discord and leave it at the iggythemad-workshop channel, or feel free to message me directly #iggythemad .

______________________________________________________________
##### CHANGELOG:

- v5.1.X: Corruption explosion fix & imbue spread fix

- v5.1.1: SoulMage soulgem bugfix & tweaks

- v5.1: Added Critical Adjustment spellslinger alternative passive.

- v5.0: ChronoMage release, custom attributes dependency implementation & optimization.

- v4.x - SoulMage & Spellslinger Rework. New Apothecary recipees. Tweaks. Bugfixes.

- v4.0 - Class Mods Merge, new Mad Whisperer 2.0, new questline, new gossip npcs, and tweaks.

- v3.0 - Apothecary skill trees, Charged attacks. TrainersOfAurai rebrand.