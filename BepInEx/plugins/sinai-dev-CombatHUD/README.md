## CombatHUD

Combat HUD provides a number of combat-related HUD tools, all of which can be toggled on/off and customized:

* Status Timers: show remaining lifespans above the icons
* Numerical Vitals: show player's HP / Stamina / Mana values as numbers
* Floating Damage Labels: With color for each damage, can customize the text size, speed, lifespan, etc
* Target Enemy Info: Status Effects, remaining Health label, and an "Enemy Infobox" to view in-depth stats

The config can be managed through the [Config Manager](https://outward.thunderstore.io/package/Mefino/Outward_Config_Manager/), or the r2modman Config Editor, or by editing the file at `BepInEx\config\com.sinai.combathud.cfg`.

## Latest updates

### 5.4.1

* Added percent values for Needs statuses instead of the infinity symbol (eg. 50% when you reach Very Hungry, etc.)

### 5.4.0

* Updated for Definitive Edition

### 5.3.3

* Fixed some error spam that probably didn't affect anything

### 5.3.2

* Fixed an issue with status timers not working in split-screen co-op

### 5.3.1

* Fixed a display error with disease status timers

### 5.3

* The player status timers now supports diseases and other status effects without a traditional timer
* Fixed some immunities from not being displayed in the infobox 
* Rewrote a lot of the mod to improve the performance and quality of the code