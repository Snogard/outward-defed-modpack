# Fast Mana Ward
- Changes casting of Mana Ward from "Bubble" to "Fast" to speed up reaction time
- Reduces Cool Down for Mana Ward

## How To Install
This is a BepInEx mod.

1. If you haven't already, download and install BepInEx: instructions here
2. Install the SideLoader first.
3. Download the Vertani-FastManaWard.zip file from the Files page
4. Put this zip file in your Outward directory, so it looks like "\BepinEx\plugins\Vertani-FastManaWard.zip"
5. Right-click the file and "Extract Here", so it merges with the folders.
6. Done.