## Dragon Knight

This class adds a new skill tree to Outward that compliments many vanilla skill trees adding new synergy builds and ways to play!

The class has an emphasis on the Fire Element, so you will find that many skills pair well with Fire Buffs and Fire Sigils.

---------- Dragon Knight (trainer in Berg) ---------- 

1. Scaled Skin: Increase Fire and hot weather resistances, but reduce frost and cold weather resistances.

2. Gigas Wave: Release a wave of flames hiting multiple targets in its wake, with a long range.

3. Cauterize Wounds: Sacrifice some health to stop any bleeding.

4. Dragon Blood: Small increase to max stamina, max mana, barrier and protection.

5. Roaring flames: Infuse your weapon with fire, set nearby enemies on fire and gain fire resistance. But also set yourself on fire.

6. Last Stand: Heal to full active health, but suffer a greater amount of damage over time. While also granting greatly increased effects for a few seconds.

6A. Bladefall: Slam your weapon to cause humongous impact and fire explotions around you

6B. Trail Blaze: Lunge forward with great speed, setting enemies hit on fire and leaving delayed explotions on the ground.

-----

The dragon knight armor is NOT included yet.

-----

Creator: Saint, ehaugw

Re-release/updates/icons: IggyTheMad (with creators permission)

Special thanks to: Sinai, ChromeTrigger, Bulldog, DigitalSaint, and doomeneka. Thank you to everyone who has supported the creation of the Dragonknight!

--- Changelog --- (Unlisted changes are just bugfixes)

v1.1 Buffed Last stand. Added new class sigil.

v1.0.1 Fixed bugs.

v1.0 Re-Release for DE
