# SideLoader Extended Effects  v1.2.4

Adds a collection of custom SL_Effect behaviours and some other Utility/Helper classes for use with SideLoader & Outward.

[Documentation](https://github.com/Grim-/SideLoader_ExtendedEffects/blob/main/Documentation.md) is always the most up-to date version of what is available, if there's an update check there or the 
[GitHub ](https://github.com/Grim-/SideLoader_ExtendedEffects)page to see whats new.

For example this library adds the SL_PlayAssetBundleVFX allowing you to spawn custom VFX Prefabs bundled in Unity.

I highly recommend you check the documentation for examples some of the new Effects added by Dan allow you to do things that are usually not possible within the Effects system framework.

For example applying a status to the player then checking with another SL_Condition if that status is on the player this allows you to generate an item on attack depending on status (and having succesfully hit a target in this case) without having to use a seperate skill to check that condition, as in the standard system conditions are evaluated once at the beginning only.

```xml
<SL_Effect xsi:type="SL_PlayAssetBundleVFX">
  <Delay>0</Delay>
  <SyncType>OwnerSync</SyncType>
  <OverrideCategory>None</OverrideCategory>
  <SLPackName>MySLPack</SLPackName>
  <AssetBundleName>MyAssetBundleName</AssetBundleName>
  <PrefabName>NameOfPrefabInAssetBundle</PrefabName>
  <PositionOffset>
      <x>0</x>
      <y>0</y>
      <z>0</z>
  </PositionOffset>
  <RotationOffset>
      <x>0</x>
      <y>0</y>
      <z>0</z>
  </RotationOffset>
  <ParentToAffected>true</ParentToAffected>
  <RotateToPlayerDirection>false</RotateToPlayerDirection>
  <LifeTime>0</LifeTime>
</SL_Effect>
```


[Emo](https://github.com/Grim-/), [Faeryn](https://github.com/Faeryn), [Dan](https://github.com/dansze) & [Pysconius](https://github.com/psyconius).


Thanks to [Avrixel](https://github.com/Avrixel/) and AlienVsYourMom of the modding discord for continually testing things for me, half of these options wouldn't exist if it weren't for them.

[ChangeLog](https://github.com/Grim-/SideLoader_ExtendedEffects/blob/main/README.md)