# Classfixes Part 1

______________________________________________________________

## Whats is this mod?

This mod reworks the Skilltrees of Kazite Spellblade , Mercenary , Philosopher , Warrior Monk , Cabal Hermit , Rogue Engineer, Wild Hunter and The Speedster by changing the skilltrees layout , swapping skills between Trainers and adding completely new and interesting skills. Also adds the new Elementalist Pistoleer trainer. You can also now use pistols in the mainhand , craft any pistol with thick oil to make the Handguns (mainhand pistols) and to reverse the change craft them with linen cloth.

## Was that even necessary?

We all enjoyed our playthoughts of Vanilla Outward but small things there, out of character thing elsewhere, lacking of option somewhere else, such things created a spark in me for what is possible for those skilltrees that could make them viable options for every combination or even make them stand on their own as main picks. Also after many requests for the ability to have pistols on main hand I finally made it possible for everyone to enjoy!

## Did you change or edited the Skills?

I tried to not edit any actual Vanilla Skill, for the most part all of them are untouched for the sake of balance and for compatibility with other mods that may edit some of their aspects. In this mod I've only edited Chakram Skills requrements, Bullet Skills, Infuse Skills and Imbue related skills. So any mod that edit those may be possibly incompatible. Also do not install together with Brute Guardian Mod, all his skills are incomporated in this mod!

## Will there be more reworked Skilltrees?

This is the Part 1 of the 3 Parts series of mods. You can find the rest of them uploaded in the Thunderstore.

______________________________________________________________

## Future plans
 More balance.

## Communication
 To report any bugs join Outward Modding Community on Discord and leave your commend on stormcancer-workshop Channel! Thanks a lot!

## Contributors
 Many thanks to Proboina, Avrixel, Rickey_Soares, Schnabeldoktor ,Breadcrumb_Lord, IggyTheMad, Dubdubaba, Ehaugw, borkmaster, Reapy_s0ul, RazielSoulshadow and deathbird for their help with their clever suggestions and ideas! They help me so much on my brainstorming! Extra thanks for deathbird who helped me with mainhand pistols!
Also huge thanks to Emo for his amazing help on overriding the vanilla skilltrees with my custom new!
Thanks to all the Outward Modding Community and to Sinai for the tools he provided us!

                       
### Enjoy! :)

______________________________________________________________


## Installation
- 1 Download [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)
- 2 Download [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)
- 3 Download [SideLoader ExtendedEffects](https://outward.thunderstore.io/package/SLExtendedEffects/SideLoader_ExtendedEffects/)
- 4 Unpack the contents of the zip file into the ...OutwardDefinitiveEd\BepInEx\plugins\stormcancer-Classfixes_Part_1

______________________________________________________________

## Spoilers Ahead!

### Whats New:


#### Trainers:

- **Avrakien**(A Elatt Follower , can be found in the Conflux Chambers, he can teach you some basic offhand skills)

- **Elementalist Pistoleer**(A new trainer that teaches all Pistol/Handgun/Bullet skills, Fire and Frost Sigil and some elemental new skills and passives, can be found in Levant near the main entrance under a statue.)

- **Fire Sage**(Trainer found somewhere in Abrassar Desert open world)

- **Frost Sage**(Trainer found somewhere in Chersonese open world)

- **Wind Sage**(Trainer found somewhere in Enmerkar open world)

- **Lightning Sage**(Trainer found somewhere in Antique Plateau open world)

- **Mana Sage**(Trainer found somewhere in Chersonese open world)

- **Plague Sage**(Trainer found somewhere in Hallowed Marsh open world)

- **Blood Sage**(Trainer found somewhere in Antique Plateau open world)

- **Light Sage**(Trainer found somewhere in Hallowed Marsh open world)

- **Smoke Sage**(Trainer found somewhere in Abrassar Desert open world)

- **Dark Sage**(Trainer found somewhere in Enmerkar open world)
______________________________________________________________

### 1. Kazite SpellBlade:

![Spellblade](https://imgur.com/mSX9eX3.png)

![Enchanted Vials Pouch](https://imgur.com/umOLbXN.png) - **Enchanted Vials Pouch**(Increases your Pouch Capacity by 5 and your Elemental Damage by 5%.)

![Fitness](https://imgur.com/2a9kwJx.png) - **Fitness**(Increases your maximum Health by 25 and the amount of Health you restore while sleeping by 20%.) (Vanilla)

![Elemental Whirling](https://imgur.com/OBJfQ3J.png) - **Elemental Whirling**(Required: Infused Weapon. Spin your weapon around you to create an elemental blast, inflicts a hex depending the active infusion. Can hit twice.)

![Spellblade's Awakening](https://imgur.com/KzEa1wY.png) - **Spellblade's Awakening**(Increases your maximum Health, Stamina and Mana by 15 each) (Vanilla)

![Elemental Shock Wave](https://imgur.com/alcAhJ7.png) - **Elemental Shock Wave**(Required: Infused Weapon. Raise your weapon high removing the elemental infusion to release an shockwave that expands inflicting elemental damage to your enemies. If your enemies have elemental hexes that match your infusion deals extra damage and consumes the hex.)

![Spell of Absorption](https://imgur.com/ohlhAEy.png) - **Spell of Absorption**(Cast a spell that damages and absorbs an element from your enemy to infuse your weapon for a short duration. The element depends on the elemental hex active on your enemy.)

![Mystical Infusions](https://imgur.com/geqYAyQ.png) - **Mystical Infusions**(Temporarily adds elemental damage to any of your equipped weapons depending the sigil you are in. Also works near revealed souls.) (Imbue Combo Opener, it overrides already active imbues. Fire Sigil=Mystic Fire Imbue, Frost Sigil=Mystic Frost Imbue, Active Soul Spot=Mystic Ethereal Imbue, Blood Sigil=Blood Imbue, Light Sigil=Divine Light Imbue, Dark Sigil=Mystic Darkness Imbue and Decay Sigil for Part3=Mystic Decay Imbue.)

______________________________________________________________

### 2. Cabal Hermit:

![Hermit](https://imgur.com/v05fu5v.png)

![Flamethrower](https://imgur.com/2M05QzH.png) - **Flamethrower**(Required: Torch, Lantern, Infused Offhand or any Lexicons. Shoots a continues blast in a line that burns your enemies.) (Reworked)

![Mana Push](https://imgur.com/BG8YEEW.png) - **Mana Push**(Spell which inflicts no damage but has High Impact in a wide area in front of the caster. Can be combined with other spells for powerful effects.) (Vanilla plus new combos)

![Weather Tolerance](https://imgur.com/40jXQRr.png) - **Weather Tolerance**(Increases your Hot and Cold Weather Defense by 8. Increases your Decay Resistance by 5.) (Vanilla)

![Shamanic Resonance](https://imgur.com/drD5zYE.png) - **Shamanic Resonance**(The effects of boons on you are increased. For example, a boon that increases your Fire Resistance by 20 would increase it by 30 instead.) (Vanilla)

![Elemental Orb](https://imgur.com/HKO5q6k.png) - **Elemental Orb**(Required: Infused Offhand or any Lexicon. Summon an Elemental Orb that hovers stationary until bursting to create a blast that damages enemies and inflicts them with the corresponding Elemental Hex.)

![Wind Walk](https://imgur.com/2UxwHkA.png) - **Wind Walk**(Requires: Activated Wind Altar. A spell that makes the caster and those around him to move faster.)

![Sigil of Wind](https://imgur.com/jf3WdlB.png) - **Sigil of Wind**(Required: Activated Wind Altar. Create a magic circle on the ground. Certain skills have additional effects when cast within this circle.) (Vanilla Combo Opener + New combo with Rotteness from Part 3)

![Call to Elements](https://imgur.com/wEZOmhr.png) - **Call to Elements**(Grants you imbues when you strike enemies with your weapons. The type of imbue depends on your Temperature state, your Corruption and Bleed state or your Souls state. Requires Activated Wind Altar.) (Reworked to an Imbue Combo Opener, it does not override already active imbues. When only Wind Altar is active=Wind Imbue, if Hot/very Hot= Mystic Fire Imbue, if Cold/Very Cold= Mystic Frost, if Corrupted/Defiled= Mystic Decay Imbue, if Dark Souls= Mystic Darkness Imbue, if Light Souls=Divine Light Imbue, if Corrupted/Defiled + Bleeding/Extreme Bleeding= Blood Imbue.)

![Call of the Earth](https://imgur.com/WMA5VIu.png) - **Call of the Earth**(Required: Infused Offhand or any Lexicon. Summon an elemental fissure thats travels in a line damaging all enemies inside. Consumes offhand infusion.)

![Conjure](https://imgur.com/0a6VCsD.png) - **Conjure**(Spell which does nothing on its own, but can be combined with other abilites.) (Vanilla plus new combos)

![Chromatic Call](https://imgur.com/f4eCA7J.png) - **Chromatic Call**(Required: Any active Boon. Summon Chromatic Projectiles that follow you and your allies and grants all the Elemental Boons you have already active.)

______________________________________________________________

### 3. Wild Hunter:

![Hunter](https://imgur.com/ctGByW3.png)

![Enrage](https://imgur.com/IkS6N9V.png) - **Enrage**(Grants the Rage boon, which increases the Impact of your attacks. Using this skill burns a bit of your maximum health until the next time you sleep. Certain skills require this boon to be active.) (Vanilla)

![Evasion Shot](https://imgur.com/7mJD53B.png) - **Evasion Shot**(Required: Bow. Shoot an arrow while jumping backwards. Causes the Cripple status effect on the target. Locking on foes is essential with this skill.) (Vanilla plus new combo)

![Seismic Thrust](https://imgur.com/GuaBQcf.png) - **Seismic Thrust**(Required: Melee Weapon. Generate a small shockwave while charging and a second stronger one while thrust around you with a big swing, grants you Rage boon if the second shockwave hits successfully. Inflicts Weaken and Confusion.)

![Hunter's Eye](https://imgur.com/bqPRemS.png) - **Hunter's Eye**(You can lock on enemies at longer distances when using a bow.) (Vanilla)

![Sniper Shot](https://imgur.com/MpXESDl.png) - **Sniper Shot**(Required: Bow. Let loose a very strong arrow shot with a long windup, making it a poor option in the middle of combat.) (Vanilla plus new combo)

![Survivor's Resilience](https://imgur.com/4uKoNCl.png) - **Survivor's Resilience**(Increases your maximum health by 40.) (Vanilla)

![Predator Leap](https://imgur.com/vdqjQRI.png) - **Predator Leap**(Jumping attack that creates a shockwave upon landing, with high Impact and Damage.) (Vanilla)

![Sagittarius' Precision](https://imgur.com/9WqhjbM.png) - **Sagittarius' Precision**(Grants Precision stacks with every successful hit of Evasion, Pierce and Triple Shot. A Precision stack reduces the stamina cost by 5% and increases Sniper Shots base damage.) (Combo Opener)

![Triple Shot](https://imgur.com/JbEy7eC.png) - **Triple Shot**(Required: Bow. Shots three arrows at once but with less accuracy. Inflicts Crippled.) (Plus combo)

![Feral Strikes](https://imgur.com/egvHYgN.png) - **Feral Strikes**(Required: Melee Weapon, Rage Boon. Attack twice, consuming your Rage boon in the process. Inflicts Extreme Bleeding and Pain.) (Vanilla)

![State of Frenzy](https://imgur.com/BJNKJ2X.png) - **State of Frenzy**(While under the effect of the Rage boon, movement speed and attack speed are increased by 10%.)

![Piercing Shot](https://imgur.com/QvTK2lO.png) - **Piercing Shot**(Required: Bow. Strong shot that goes through enemies and hits other foes behind. Inflicts Extreme Bleeding.) (Vanilla plus new combo)

______________________________________________________________

### 4. Warrior Monk:

![Monk](https://imgur.com/o7GlwaE.png)

![Slow Metabolism](https://imgur.com/gQ0ypsG.png) - **Slow Metabolism**(It takes more time for you to become hungry, thirsty and sleepy.) (Vanilla)

![Focus](https://imgur.com/7kCoKSZ.png) - **Focus**(Grants the discipline boon, which increases the physical damage of your attacks. Using this skill burns a bit of your maximum stamina until the next time you sleep. Certain skills require this boon to be active.) (Vanilla)

![Drop Kick](https://imgur.com/OG62iMf.png) - **Drop Kick**(A jump kick attack that deals massive Impact. Inflicts Confusion.)

![Brace](https://imgur.com/qiQVn8b.png) - **Brace**(Blocks an attack, restoring your stability and giving you the Discipline boon. All nearby enemies are knocked back.) (Vanilla but also useable with bows)

![Healing Light](https://imgur.com/dpue5Ly.png) - **Healing Light**(A spell that calls upon the light to create healing effects. Can be used in combination with other spells for greater effects.) (Plus combos)

![Steadfast Ascetic](https://imgur.com/tJrmG2l.png) - **Steadfast Ascetic**(Increases your maximum Stamina by 40.) (Vanilla)

![Perfect Strike](https://imgur.com/VFctczA.png) - **Perfect Strike**(Required: Any Melee Weapon, Discipline Boon. Lunging attack that ignores all defenses and inflicts Pain.) (Vanilla plus combo)

![Unbreakable Guard](https://imgur.com/YTcjeff.png) - **Unbreakable Guard**(A unbreakable stance that guards you and mitigates any incoming damage for its duration.)

![Master of Motion](https://imgur.com/r9RkHzW.png) - **Master of Motion**(While under the effect of the Discipline boon, Impact Resistance and all damage resistances are increased by 10.) (Vanilla)

![Light Bringer](https://imgur.com/vPMuDoC.png) - **Light Bringer**(Increase the Electric damage you inflict by 15%. Consuming revealed souls with spark now grants you Light Souls, also unlocks the ability to consume Holy Blaze with Perfect Strike and Flash Onslaught from enemies to create healing orbs.) (Combo Opener)

![Flash Onslaught](https://imgur.com/fEeLUfq.png) - **Flash Onslaught**(Required: Any Melee Weapon, Discipline Boon Attack all enemies within a close distance with increased Damage and Impact. Using the skill consumes the Discipline boon.) (Vanilla plus combos, also smaller Cooldown.)

![Light Sigil](https://imgur.com/cye1iT8.png) - **Light Sigil**(Requires: Light Souls. Create a Light Sigil on the ground. Certain skills have additional effects when cast within this circle.) (Combo Opener, can combo with Healing Light,Spark,Mana Push and Conjure + New combo with Rotteness from Part 3)

______________________________________________________________

### 5. Philosopher:

![Philosopher](https://imgur.com/9JYGcMg.png)

![Chakram Pierce](https://imgur.com/aJ5WjiQ.png) - **Chakram Pierce**(Required: Chakram. Launch the disc forward, possibly hitting the enemy twice.) (Vanilla but changed requirements)

![Mana Ward](https://imgur.com/It4GZSc.png) - **Mana Ward**(Envelops you in Mana and, for an instant, you become immune to Damage. Can be combined with other spells for powerful effects.) (Vanilla plus new combos)

![Intuition](https://imgur.com/V2MKP1D.png) - **Intuition**(Grants the Clairvoyance boon, which decreases the Mana Cost of your Skills. Using this skill burns a bit of your maximum mana. Certain skills require this boon to be active.)

![Reveal Soul](https://imgur.com/WmF5bWF.png) - **Reveal Soul**(Reveal the blueish soul orb left by deceased thieves and adventurers. Certain spells must be cast within proximity of a soul.) (Vanila Combo Opener plus new combos)

![Force Portal](https://imgur.com/D9vLnPJ.png) - **Force Portal**(Spell which inflicts no damage but teleports your enemy away from you while giving you the Clairvoyance boon.)

![Leyline Connection](https://imgur.com/vdiahYP.png) - **Leyline Connection**(You regain 0.34 mana per second. This is a constant effect.) (Vanilla)

![Chakram Arc](https://imgur.com/Uo54ZkQ.png) - **Chakram Arc**(Required: Chakram, Clairvoyance Boon. The disc sweeps the area in front of the caster, dealing high Damage and Impact. Clairvoyance is required to cast this spell.) (Vanilla but changed requirements)

![Inner Darkness](https://imgur.com/SO8txwS.png) - **Inner Darkness**(Increase the Ethereal damage you inflict by 15%. Consuming revealed souls with spark now grants you Dark Souls, also unlocks unlocks new effects for Force Portal and reduces its cooldown significantly.) (Combo Opener)

![Dreamer's Pipe](https://imgur.com/PRX732w.png) - **Dreamer's Pipe**(Required: Boiled Dreamer's Root. Grants the Dreamer's Pipe status, which decreases Mana cost by 20% and Physical Resistance and Impact Resistance by 15%.)

![Chakram Dance](https://imgur.com/e88HbKW.png) - **Chakram Dance**(Required: Chakram, Clairvoyance Boon. The disc sways left and right as it advances, hitting multiple times and dealing high damage. Discipline is required to cast this spell) (Vanilla but changed requirements)

![Master of Mind](https://imgur.com/Kj50TBi.png) - **Master of Mind**(While under the effect of the Clairvoyance boon, Barrier is increased by 4 and Mana Regeneration by 0.1.)

![Dark Sigil](https://imgur.com/4JxcY31.png) - **Dark Sigil**(Requires: Dark Souls. Create a Dark Sigil on the ground. Certain skills have additional effects when cast within this circle.) (Combo Opener, can combo with Spark, Healing Light, Mana Ward and Conjure + New combo with Rotteness from Part 3)

______________________________________________________________

### 6. Elementalist Pistoleer:

![Elementalist](https://imgur.com/iD7dZGX.png)

![Shatter Bullet](https://imgur.com/SU28j0g.png) - **Shatter Bullet**(Required: Pistol/Handgun, 1 Iron Scraps. Load a scap bullet with high Impact which also inflicts Pain. Use Gun Shot to shoot that scrap bullet that deals high impact consuming Shatter Bullet.) (New custom version of the skill that doesn't work like an imbue anymore, Vanilla is still accessable via Debug)

![Gun Shot](https://imgur.com/q1ullR7.png) - **Gun Shot**(Required: Pistol/Handgun and Bullet. Reload and fire a bullet from your equipped pistol/handgun, also works with imbues and bullet skills.) (Better Custom Shoot/Reload that works with Imbues, new Bullet skills and the new mainhand pistols called Handguns , the vanilla Shoot/Reload still exists but its only useful to simply shoot offhand pistols and nothing else.)

![Frost Bullet](https://imgur.com/uLE2Mzk.png) - **Frost Bullet**(Required: Pistol/Handgun, Cool Boon. Absorb the enemy’s cold into your pistol as a frost bullet. Use Gun Shot to shoot a frost bullet that deals frost damage and inflicts Slow Down and Cripple while consuming Frost Bullet.) (New custom version of the skill that doesn't work like an imbue anymore, Vanilla is still accessable via Debug)

![Swift Foot](https://imgur.com/10Azjk5.png) - **Swift Foot**(Your movement speed is increased by 10%.) (Vanilla)

![Sigil of Fire](https://imgur.com/BxhGVty.png) - **Sigil of Fire**(Consumes: Fire Stone. Create a magic circle on the ground. Certain skills have additional effects when cast within this circle.) (Vanilla Combo Opener + New combos with Mana Push and Conjure)

![Marathoner](https://imgur.com/C6HpW2W.png) - **Marathoner**(Sprinting consumes 40% less stamina.) (Vanilla)

![Heat Bullet](https://imgur.com/Xp550rg.png) - **Heat Bullet**(Required: Pistol/Handgun, Warm Boon. Absorb the enemy’s heat into your pistol/handgun as a heat bullet. Use Gun Shot to shoot a magma bullet that deals fire damage and inflicts Burning on all near enemies and consuming Heat Bullet.)

![Elemental Invocation](https://imgur.com/OHx99MA.png) - **Elemental Invocation**(A Spell that calls upon the elemental spirits. Requires Sigils to activate its effects.) (Combos with any sigils: Fire Sigil=Fire Elemental, Frost Sigil=Frost Elemental, Blood Sigil=Blood Elemental, Light Sigil=Light Elementale, Dark Sigil=Dark Elemental and Decay Sigil for Part3=Decay Elemental.

![Sigil of Ice](https://imgur.com/uKtrDGS.png) - **Sigil of Ice**(Consumes: Cold Stone. Create a magic circle on the ground. Certain skills have additional effects when cast within this circle.) (Vanilla Combo Opener + New combos with Conjure and Rotteness from Part 3)

![Blood Bullet](https://imgur.com/KkA5PXx.png) - **Blood Bullet**(Required: Pistol/Handgun, Possessed Boon. Absorb the enemy’s life into your pistol/handgun as a blood bullet, healing yourself. Use Gun Shot to shoot a blood bullet that deals decay damage consuming Blood Bullet.) (New custom version of the skill that doesn't work like an imbue anymore, Vanilla is still accessable via Debug)

![Elemental Apotheosis](https://imgur.com/zqsLiND.png) - **Elemental Apotheosis**(Summoning any sigil grants you 10% elemental damage and 50 resistance to its corresponding element while also lowering 50 resistance to its opposite element. You can only have one Apotheosis buff at a time.)

![Fire Affinity](https://imgur.com/OabJjZG.png) - **Fire Affinity**(Increase your resistance to Fire by 20 and the Fire damage you inflict is increased by 15% but you are more prone to Heat Temperature. Now casting Fire Sigil while you are Hot/Very Hot consumes some of your Heat instead of your Fire Stone, but you still need to carry one.) (Reworked)

![Frost Affinity](https://imgur.com/XdmI6rO.png) - **Frost Affinity**(Increase your resistance to Frost by 20 and the Frost damage you inflict is increased by 15% but you are more prone to the Cold Temperature. Now casting Ice Sigil while you are Cold/Very Cold consumes some of your Cold instead of your Cold Stone, but you still need to carry one.)

______________________________________________________________

### 7. Mercenary:

![Mercenary](https://imgur.com/YYWYRbW.png)

![Shield Charge](https://imgur.com/nByBvgd.png) - **Shield Charge**(Required: Shield. Rush forward to hit foes with your shield for high Impact. Can hit the target twice.) (Vanilla)

![Bashing Strikes](https://imgur.com/0HVtxNm.png) - **Bashing Strikes**(Required: Any Melee Offhand Weapon. A weak bash attack that increases Tenacity level on hit. Evolves into stronger forms as Tenacity level rises. The last evolution requires Confusion to apply Dizzy on enemies.) (Combo Opener)

![Power Style](https://imgur.com/t6lpiPC.png) - **Power Style**(A weak kick that increases Tenacity level on hit. Evolves into stronger forms as Tenacity level rises.) (Combo Opener)

![Fast Maintenance](https://imgur.com/GzKzzI8.png) - **Fast Maintenance**(You repair your equipment 50% faster when you allocate time to Repairing in the rest menu.) (Vanilla)

![Shield Infusion](https://imgur.com/TboER9z.png) - **Shield Infusion**(Required: Shield. Block an elemental attack and infuse your shield with that element.) (Reworked to infuse the shield with mystic imbues.)

![Steady Arm](https://imgur.com/Uh84Kt5.png) - **Steady Arm**(Increases Impact Resistance by 10 while you are blocking.) (Vanilla)

![Mercenary's Tenacity](https://imgur.com/VYYFZel.png) - **Mercenary's Tenacity**(Increases Protection and Impact Resistance per Tenacity Level)

![Shield Throw](https://imgur.com/JYOVfbL.png) - **Shield Throw**(Required: Infused Shield. Launch your infused shield forward, possibly hitting the enemy twice.)

![Direct Momentum](https://imgur.com/Kqizl6O.png) - **Direct Momentum**(Removes 3 Tenacity Levels, Perform an instant flying kick leaping towards your enemy. Deals high Physical Damage.)

![Armor Training](https://imgur.com/ZkiP0Oe.png) - **Armor Training**(Decreases the stamina and movement penalties from wearing armor by 50%.) (Vanilla)

![Gong Strike](https://imgur.com/fWFiIBA.png) - **Gong Strike**(Required: Infused Shield. Strike your weapon on your shield, removing the elemental infusion to make an explosion of that element.) (Vanilla but requires and consumes offhand imbue.)

![Counterstrike](https://imgur.com/aMT62H8.png) - **Counterstrike**(Required: Any Melee Weapon. Removes 2 Tenacity Levels, completely block a physical attack, striking the attacker and dealing high Damage.) (Reworked to new requirements.)

![Shield Guardian](https://imgur.com/b3WbjvP.png) - **Shield Guardian**(Increases your Protection by 4 but adds +8% Stamina Cost.)

![Pure Muscles](https://imgur.com/WGai0Hp.png) - **Pure Muscles**(Increases your Impact and your Physical Damage by 20% but sacrifices 50% of your Max Mana.)

______________________________________________________________

### 8. Rogue Engineer:

![Rogue](https://imgur.com/b8sLi9o.png)

![Backstab](https://imgur.com/enmlvRw.png) - **Backstab**(Required: Dagger. Dagger attack. If the attack hits the enemy's back, the damage and impact are tripled.) (Vanilla)

![Sweep Kick](https://imgur.com/0YFReqe.png) - **Sweep Kick**(Weak attack with wide arc of effect and high impact. Will knock down a target afflicted by Confusion status effect.) (Vanilla)

![Pressure Plate Training](https://imgur.com/wblrhBW.png) - **Pressure Plate Training**Enable you to deploy complex Pressure Plate Traps. These traps are primarily armed with charges, which shoot poison, fire and more.) (Vanilla)

![Opportunist Stab](https://imgur.com/t6pIGe9.png) - **Opportunist Stab**(Required: Dagger. Dagger attack. If the enemy has the Pain or Confusion hex, this attack's damage and knockback are tripled. If it has both, it is 6 times stronger instead.) (Vanilla)

![Feather Dodge](https://imgur.com/AHJHibs.png) - **Feather Dodge**(Lowers the Stamina cost of dodging by 50% and allows you to dodge unimpeded even when wearing a backpack.) (Vanilla)

![Serpent's Parry](https://imgur.com/EjhnDMA.png) - **Serpent's Parry**(Required: Dagger. Counter a physical attack with a strong dagger strike. Targets afflicted by Confusion are briefly stunned. Targets afflicted by Pain will be inflicted Extreme Bleeding.) (Vanilla)

![Stealth Training](https://imgur.com/C7vHwk2.png) - **Stealth Training**(Enemies will have a harder time spotting you. Sprinting and having a light source will still easily alert them, though) (Vanilla)

![Pressure Plate Expertise](https://imgur.com/5iKWOir.png) - **Pressure Plate Expertise**(Increases the power of your Pressure Plate Traps. Allows reuse of Pressure Plates after they have been triggered, though they must be re-armed with another charge or compatible item.) (Vanilla)

![Dagger Throw](https://imgur.com/njUcKqr.png) - **Dagger Throw**(Required: Dagger. Ranged dagger attack inflicting minor damage and Fragility on your target.)

![Smokescreen](https://imgur.com/vOPlCiK.png) - **Smokescreen**(Requires: Thick Oil. Thow a mixture that creates a smoke cloud blinding any enemy that enters it.)

![Plate Plant](https://imgur.com/HRfhmX2.png) - **Plate Plant**(Requires: Pressure Plate. Plants a presure plate on your enemy. Elemental debuffs such as burning will trigger it and send it into a count down that will unleash a corresponding elemental blast.)

______________________________________________________________

### 9. The Speedster:

![Speedster](https://imgur.com/LOU1W2s.png)

![Efficiency](https://imgur.com/SUbofCU.png) - **Efficiency**(Maximum Stamina increased by 25.) (Vanilla)

![Probe](https://imgur.com/Oq9tLPj.png) - **Probe**(A weak attack that increases Alertness level on hit. Evolves into stronger forms as Alertness level rises.) (Vanilla Combo Opener)

![Swift Style](https://imgur.com/yjxVpkf.png) - **Swift Style**(A weak kick that increases Alertness level on hit. Evolves into stronger forms as Alertness level rises.) (Combo Opener)

![Metabolic Purge](https://imgur.com/lMORNgz.png) - **Metabolic Purge**(Passive skill that increases Corruption resistance by 10%.) (Vanilla)

![Daredevil](https://imgur.com/3mMlY1r.png) - **Daredevil**(Reduce all skill Cooldowns by 10% per Alertness level.) (Vanilla)

![Prime](https://imgur.com/wOYKpW4.png) - **Prime**(Removes 2 Alertness levels, instantly resets the cooldown of the next skill used in the next 3 minutes.) (Vanilla)

![Aerial Maneuver](https://imgur.com/CH61ggO.png) - **Aerial Maneuver**(Removes 2 Alertness Levels, Perform an instant backflip while kicking your enemy. Deals high Impact.)

![Unerring Read](https://imgur.com/p6SPBVz.png) - **Unerring Read**(Removes 1 Alertness levels, cancels the next hit received in the next 20 seconds.) (Vanilla)

![Blitz](https://imgur.com/KzOYkwx.png) - **Blitz**(Increases Sprinting speed by 5% and running attack damage by 10% per Alertness level.) (Vanilla)

![Anticipation](https://imgur.com/OFkNPnK.png) - **Anticipation**(Increases physical protection stat by 2 per Alertness level.) (Vanilla)

______________________________________________________________

#### Sage's Skills:

![Sage's Infuse Fire](https://imgur.com/b8cam0C.png) - **Sage's Infuse Fire**(Temporarily adds Fire damage to your weapons and inflicts Burning on enemies.)

![Sage's Infuse Frost](https://imgur.com/Dg3pncN.png) - **Sage's Infuse Frost**(Temporarily adds Frost damage to your weapons and inflicts Frostbite on enemies.)

![Sage's Infuse Wind](https://imgur.com/XsHoNKp.png) - **Sage's Infuse Wind**(Increases the Impact and Attack Speed of your weapons, however your stamina burn increases more quickly.)

![Sage's Infuse Lightning](https://imgur.com/extsVxY.png) - **Sage's Infuse Lightning**(Temporarily adds Lightning Damage to your weapons and inflicts Weaken on enemies)

![Sage's Infuse Mana](https://imgur.com/xZEG8lT.png) - **Sage's Infuse Mana**(Temporarily adds Ethereal damage to your weapons and inflicts Aetherbomb on enemies.)

![Sage's Infuse Plague](https://imgur.com/0iOYXfK.png) - **Sage's Infuse Plague**(Temporarily adds Decay damage to your weapons and inflicts Extreme Poison and Plague on enemies.)

![Sage's Infuse Blood](https://imgur.com/BmW8I8N.png) - **Sage's Infuse Blood**(Infuses your weapons with the Decay element, as well as Extreme Poison and Extreme bleeding. Leeches 10% of damage dealt into your HP. Costs 20 Life to activate.)

![Sage's Infuse Light](https://imgur.com/41VTW6y.png) - **Sage's Infuse Light**(Temporarily adds Lightning Damage to your weapons and inflicts Holy Blaze on enemies.)

![Sage's Infuse Smoke](https://imgur.com/xAd2pX6.png) - **Sage's Infuse Smoke**(Temporarily adds Raw damage to your weapons and inflicts Sapped on enemies.)

![Sage's Infuse Darkness](https://imgur.com/euWXzed.png) - **Sage's Infuse Darkness**(Temporarily adds Ethereal damage to your weapons and inflicts Disintegrate on enemies.)

(Be warned, Elemental Sages are extremely hard to find, hidden in unimaginable spots of each openworld. Especially Dark Sage requires you to "somehow" get out of the games boundaries and look for an impossible to otherwise go point of interests that exist in the Enmerkar map. Also unless you are somehow ready to pay 9999 silver, you need to have the right Class already to be able to pick the corresponding Infuse skill. No more spoils :P Happy hunting!)
______________________________________________________________

#### Items:

- **Clairvoyance Potion**(Recipe sold by every Alchemist except Levants)

- **Pressure Plate Trap Recipe**(Recipe sold by every Blacksmith)

- **Reset Custom Passives**(Use after you removed any custom passive, then restart the game.)(Works for all Classfixes Custom Passives)(Only for debug use)

______________________________________________________________

#### Handguns:

- **Flintlock Handgun**

- **Cannon Handgun**

- **Ornate Handgun**

- **Manticore Handgun**

- **Savage Handgun**

- **Obsidian Handgun**

- **Horror Handgun**

- **Galvanic Handgun**

- **Cage Handgun**

- **Kazite Handgun**

- **Wolf Handgun**

- **Astral Handgun**

- **Forged Glass Handgun**

- **Chalcedony Handgun**

- **Smoke Handgun**

- **Militia Handgun**

- **Vigilante Handgun**

- **Meteoric Handgun**

- **Nightmare Handgun**

- **Alpha Handgun**

- **Vampiric Handgun**

- **Nosferatus Handgun**

- **Hailfrost Handgun**

______________________________________________________________