## Tsar Stone Crafting

In the base game of Outward, a single playthrough can only allow a player to gather 5 Tsar Stones, which is something I vehemently disagree with. I created this mod in order to patch the hole left in the end-game attainment of Tsar weapons and armor.

This mod adds 1 new item and 2 new recipes. 

Combining ANY 4 Elemental Particles in an Alchemy Lab creates a single Elemental Energy.

In order to create a Tsar Stone, you need 2 Elemental Energy, 1 Hackmanite and 1 Large Emerald, crafted also at an Alchemy Station.


### Is This Balanced?

I'd like to believe so. Tsar Stones in the base game have no renewable method of collection, and that basically leaves gimmicks like the legacy chest or co-op/splitscreen item duplication. Both of those options leave much to be desired, and I don't like the idea of requiring mods like New Game Plus in order to get my Tsar equipment, so I made this mod to provide a semi-lore friendly method of crafting Tsar Stones.

Elemental Particles aren't exactly common ingredients, as the enemies that drop them are not very common in the first place. Not to mention the fact that you need 4 particles for a single energy, and you need 2 energy per Tsar Stone. Particles are also used for other stuff, mainly incenses for enchantments, so if you're digging into your supply of particles for Tsar Stones, you won't be using them for enchanting and vice versa.


## Latest changes

* `1.0` - added elemental energy and the recipe for both elemental energy and tsar stones to the game

