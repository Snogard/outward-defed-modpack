## Blacksmith's Toolbox

The Blacksmith's Toolbox is a simple item which allows you to repair your items while adventuring. It can be purchased from any Blacksmith vendor (requires a stock reset for item to appear for the first time), and consumes iron scrap on use. 

You can configure all costs (silver cost, scrap per use and durability lost per use) through a Config Manager or the r2modman config editor, or directly at the config file `BepInEx\config\com.sinai.blacksmithstoolbox.cfg`.