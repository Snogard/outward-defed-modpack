# Armory Additions~

______________________________________________________________

## Whats is this mod?

 This mod aims to add new armors and new Legacies for existing armor sets! Also now all Legacy armor sets (both new and vanilla) can be crafted by combining their non legacies and relics!
 
## You want more Weapons too?
		   
 Also if you want the new weapons and armors or new interesting effects aswell download "Weaponry and Enchantments Additions", "Knives Master" and "WeaponBalancePatch" too. I recommend them for a full weaponry experience. ;)

______________________________________________________________

## Future plans
 Add more armors!

## Communication
 To report any bugs join Outward Modding Community on Discord and leave your commend on #stormcancer-workshop Channel! Thanks a lot!

## Contributors
 Many thanks to Proboina for his contributions on Nightmare Set, many thanks to Zipper for his contributions on Tsar Set and many thanks to all the Outward Modding Community for their help!


### Enjoy! :)
______________________________________________________________


## Installation
- 1 Download [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)
- 2 Download [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)
- 3 Download [SideLoader ExtendedEffects](https://outward.thunderstore.io/package/SLExtendedEffects/SideLoader_ExtendedEffects/)
- 4 Unpack the contents of the zip file into the ...OutwardDefinitiveEd\BepInEx\plugins\stormcancer-Armory_Additions

______________________________________________________________

## Spoilers Ahead!

### New Items:

#### New Armors:

![Imgur](https://imgur.com/4PMpbxK.gif)
- **Black Cultist Armor Set**(Sold by Alchemists.)
______________________________________________________________
![Imgur](https://imgur.com/6Amnj9v.gif)
- **Obsidian Plate Armor Set**(Recipe sold by Blacksmiths.)
______________________________________________________________
![Imgur](https://imgur.com/dXlqyTj.gif)
- **Chalcedony Helmet**(Changed the vanilla with a new custom one.)
______________________________________________________________
- **Jester Mask**(Sold by Alchemists.)
______________________________________________________________
![Imgur](https://imgur.com/xqOHJfH.gif)
- **Tuanosaur Armor Set**(Recipe sold by Blacksmiths.)
______________________________________________________________
- **Merton’s Set**(Reworked the Vanilla stats.)
______________________________________________________________
![Imgur](https://imgur.com/cqiqQwG.gif)
- **Jade-Lich Set**(Reworked the Vanilla set.)
______________________________________________________________
- **Gold-Lich Set**(Reworked the Vanilla stats.)
______________________________________________________________
- **Pearlbird Feathers**(Crafted by breaking down a Pearlbird Mask.)
______________________________________________________________
![Imgur](https://imgur.com/EOIat4I.gif) 
- **Pearlbird Set**(Crafted with any Looter/Scavenger/Brigand piece and Pearlbird Feathers.)
______________________________________________________________
![Imgur](https://imgur.com/JY8FLNr.gif) 
- **Chitin Set**(Recipe sold from Smooth the Tailer of Levant)
______________________________________________________________
![Imgur](https://imgur.com/O5abYXu.gif) 
- **Tsar Set**(Reworked Vanilla set, decrafting each piece changes its properties.)
______________________________________________________________
#### Legacies:

![Imgur](https://imgur.com/hfsKgh9.gif) 
- **Divine Plate Armor Set**(Legacy for Candle Plate Armor Set or combined with Elatt's Relic.)
______________________________________________________________
![Imgur](https://imgur.com/IzmhvMS.gif) 
- **Deepfreeze Plate Armor Set**(Legacy for Crimson Plate Armor Set or combined with Leyline Figment.)
______________________________________________________________
![Imgur](https://imgur.com/GC21Uaa.gif) 
- **Alpha Plate Armor Set**(Legacy for Wolf Plate Armor Set or combined with Calixa's Relic.)
______________________________________________________________
![Imgur](https://imgur.com/aAHkIgP.gif) 
- **Vampiric Plate Armor Set**(Legacy for Zagis' Armor Set or combined with Flowering Corruption.)
______________________________________________________________
![Imgur](https://imgur.com/PdSATM0.gif) 
- **Nightmare Plate Armor Set**(Legacy for Horror Plate Armor Set or combined with Scourge's Tears.)
______________________________________________________________
![Imgur](https://imgur.com/BqK6Dmv.gif) 
- **Sanguine Armor Set**(Legacy for Tenembrous Armor Set or combined with Vendavel's Hospitality.)
______________________________________________________________
![Imgur](https://imgur.com/eKg63Wo.gif) 
- **Elder Priest Armor Set**(Legacy for White Priest Armor Set or combined with Elatt's Relic.)
______________________________________________________________
![Imgur](https://imgur.com/xORKDyN.gif) 
- **Elder Cultist Armor Set**(Legacy for Black Cultist Armor Set or combined with Haunted Memory.)
______________________________________________________________
![Imgur](https://imgur.com/KXD9LbP.gif) 
- **Gaia's Plate Armor Set+Hood**(Legacy for Elite Plate Armor Set+Hood or combined with Flowering Corruption.)
______________________________________________________________
![Imgur](https://imgur.com/t94aX9u.gif) 
- **Meteoric Plate Armor Set**(Legacy for Obsidian Plate Armor Set or combined with Leyline Figment.)
______________________________________________________________
![Imgur](https://imgur.com/JGWb6i8.gif) 
- **Golden Sand Armor Set**(Legacy for Blue Sand Armor Set or combined with Leyline Figment.)
______________________________________________________________
![Imgur](https://imgur.com/VmTnxDr.gif)
-**Mithril Armor Set**(Legacy for Adventurer's Armor Set or combined with Metalized Bones.)
______________________________________________________________
![Imgur](https://imgur.com/GPMA41o.gif) 
- **Ancient Armor Set+Masks**(Legacy for Entombed Armor Set + Levant's Masks or combined with Gep's Generosity.)
______________________________________________________________
![Imgur](https://imgur.com/T1x6SsI.gif) 
- **Forge Armor Set**(Legacy for Chalcedony Armor Set or combined with Scarlet Whisper.)
______________________________________________________________
![Imgur](https://imgur.com/xBtK8OY.gif) 
- **Blood Armor Set**(Legacy for Virgin Armor Set or combined with Flowering Corruption.)
______________________________________________________________
![Imgur](https://imgur.com/oXjTiCU.gif) 
- **Illuminating Armor Set**(Legacy for Palladium Armor Set or combined with Gep's Generosity.)
______________________________________________________________
![Imgur](https://imgur.com/0W17LyY.gif) 
- **Blaze Armor Set**(Legacy for Petrified Wood Armor Set or combined with Leyline Figment.)
______________________________________________________________
![Imgur](https://imgur.com/gYQKSSE.gif) 
- **Blight Armor Set**(Legacy for Ammolite Armor Set or combined with Scourge's Tears.)
______________________________________________________________
![Imgur](https://imgur.com/kSLQwXU.gif)
- **Glacial Armor Set**(Legacy for Tuanosaur Armor Set or combined with Leyline Figment.)
______________________________________________________________
![Imgur](https://imgur.com/7G0WZIA.gif) 
- **Merton’s Wrath Set**(Legacy for Merton’s Set or combined with Vendavel's Hospitality.)
______________________________________________________________
![Imgur](https://imgur.com/qY1kH1L.gif) 
- **Scourge Acolyte Set**(Legacy for Jade Acolyte Set or combined with Scourge's Tears.)
______________________________________________________________
![Imgur](https://imgur.com/D2HZgeg.gif) 
- **Blood-Lich Set**(Legacy for Jade-Lich Set or combined with Flowering Corruption.)
______________________________________________________________
![Imgur](https://imgur.com/LPFs6pO.gif) 
- **Ice-Lich Set**(Legacy for Gold-Lich Set or combined with Enchanted Mask.)
______________________________________________________________
![Imgur](https://imgur.com/6jSvLmV.gif) 
- **Revenant Set**(Legacy for Scarlet Set or combined with Haunted Memory.)
______________________________________________________________
![Imgur](https://imgur.com/guEmS05.gif) 
- **Metal Lich Set**(Legacy for Rust Lich Set or combined with Metalized Bones.)
______________________________________________________________
![Imgur](https://imgur.com/pnPVEMw.gif) 
- **Shady Trader Set**(Legacy for Trader Set or combined with Noble's Greed.)
______________________________________________________________
![Imgur](https://imgur.com/gFxrxZY.gif) 
- **Wild Trader Set**(Legacy for Master Trader Set or combined with Pearlbird's Courage.)
______________________________________________________________
![Imgur](https://imgur.com/CaDUPRQ.gif)
- **Spellbind Plate Set**(Legacy for Antique Plate Set or combined with Haunted Memory.)
______________________________________________________________
![Imgur](https://imgur.com/Hf55wQK.gif) 
- **Witch Hunter Set**(Legacy for Silver Set or combined with Calixa's Relic.)
______________________________________________________________
![Imgur](https://imgur.com/2JOpng2.gif) 
- **Black Pearlbird Set**(Legacy for Pearlbird Set or combined with Pearlbird's Courage.)
______________________________________________________________
![Imgur](https://imgur.com/e2naO7T.gif) 
- **Jewel Bird Set**(Legacy for Black Pearlbird Set or combined with Pearlbird's Courage.)
______________________________________________________________
![Imgur](https://imgur.com/NDjbbJt.gif) 
- **Ascended Tsar Set**(Legacy for Tsar Set or combined with Calixa's Relic, decrafting each piece changes its properties.)
______________________________________________________________
#### Vanilla Legacies:

- **White Arcane Set**(Legacy for Arcane Set or combined with Enchanted Mask.)

- **Manawall Set**(Legacy for Barrier Set or combined with Leyline Figment.)

- **Orichalcum Set**(Legacy for Black Plate Set or combined with Calixa's Relic.)

- **Red Clansage Set**(Legacy for Clansage Set or combined with Enchanted Mask.)

- **Green Copal Set**(Legacy for Copal Set or combined with Flowering Corruption.)

- **Master Desert Set**(Legacy for Desert/Elite Desert Set or combined with Gep's Generosity.)

- **Black Fur Set**(Legacy for Fur Set or combined with Pearlbird's Courage.)

- **Master Kazite Set**(Legacy for Kazite Set or combined with Vendavel's Hospitality.)

- **White Kintsugi Set**(Legacy for Kintsugi Set or combined with Elatt's
Relic.)

- **Shadow Light Kazite Set**(Legacy for Light Kazite Set or combined with Vendavel's Hospitality.)

- **Vigilante Set**(Legacy for Militia Set or combined with Calygrey's Wisdom.)

- **Shock Set**(Now a Legacy for Chitin Set or combined with Calixa's Relic.)

______________________________________________________________
