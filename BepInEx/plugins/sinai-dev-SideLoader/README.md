## Outward SideLoader

The Outward SideLoader is an API and a mod development toolkit for creating and editing custom content.

Please view the **[SideLoader Docs](https://sinai-dev.github.io/OSLDocs/#/)** for full documentation on SideLoader.

### Latest changes

#### 3.8.4

* Fixed a bug with custom Status Effect icons when applied from C#

#### 3.8.3

* [SL Menu] Added "Paste Player Position" and "Paste Player Rotation" buttons for Vector3 values 

#### 3.8.2

* [SL Menu] Fixed an issue caused by closing an inspector tab in the SL Menu

#### 3.8.1

* [SL Menu] Fixed an issue with the SL Menu where some members would be cut off at the bottom of the scroll view
* [SL Menu] Removed a leftover debug log

#### 3.8.0

* Updated the SL Menu interface
  * Got rid of the page system on the main inspector
  * Updated AutoCompletes to be more responsive and less laggy
  * `enum`-type editors to use the new AutoCompleter instead of a dropdown
* Fixed some error spam that could happen with certain blasts with SplitableSoundSource components

#### 3.7.13

* Fixed an issue with multiple templates editing the same original item

#### 3.7.12

* Fix visibility mistake on SL_AttackTypeCondition

#### 3.7.11

* Fixed a bug with AffectStat effects which have no duration set

#### 3.7.10

* Added `SL_AffectStat.Duration`
* Improved logging when there are errors in the XML document
* Improved XML serialization efficiency

#### 3.7.9

* Skill Tree rows are now sorted before being applied to avoid issues with skill dependencies

#### 3.7.8

* Fixed an issue with certain blasts and projectiles having incorrect positions for some reason

#### 3.7.7

* Fixed a mistake with custom statuses and imbues where their icons were not being applied in some cases

#### 3.7.6

* Fixed cooldown reduction on equipment being flipped when generated from the template item

#### 3.7.5

* Fixed an issue with custom characters not receiving status buildup from attacks

#### 3.7.4

* Fixed a bug with custom characters not taking damage in some situations

#### 3.7.3

* Fixed an issue with item visuals

#### 3.7.2

* Fixed some exceptions with XML serialization after the DE update

#### 3.7.1

* Updated for the Definitive Edition

#### 3.7.0

* Custom Keybindings will no longer trigger when the Chat Panel is focused.
* All keybindings will now be revealed in the in-game Keybind menu (including hidden vanilla keybinds).
* Made some parts of the `ContentTemplate` class `protected internal` instead of `public` which were supposed to be for internal use only.

#### 3.6.11

* Fixed an issue with item visuals not being set correctly

#### 3.6.10

* Fixed an issue with custom keybindings

#### 3.6.7

* Fixed an issue with the ExtraQuickslots mod.

#### 3.6.6
* Fixed an issue when setting a different `ResourcesPrefabPath` for item visuals than what the item originally had
* Added `SL_AnimationOverride` effect

#### 3.6.5

* Added `SL_Quest.OnQuestLoaded` helper event

#### 3.6.4

* Added `CustomQuests` C# helper for creating custom `QuestEvent`s
* Fixed a minor issue with `SL_QuestProgress` not creating the `QuestTree` graph correctly

#### 3.6.3

* Added `SL_Quest` and `SL_QuestProgress` (just small helpers, proper quest support requires manual C# code)

#### 3.6.2

* Added new Item templates: `SL_SelfFilledItemContainer`, `SL_Gatherable` and `SL_TreasureChest`
* Added new ItemSpawn template: `SL_ItemContainerSpawn`
* Added a method to `SL_DropTable` to turn it into a `Dropable` and apply it to a `GameObject`
* It is now possible to set custom item visuals on items which didn't have any original visuals, either by setting the ResourcesPrefabPath, or SL will create a new empty visual.
* Fixed a niche bug with `TemplateDependancySolver`
* Fixed `CustomKeybindings` trying to set up keybindings after they've already been set up (harmless log spam)

#### 3.6.1

* Fixed a bug with custom SkillTree icons not being loaded until hovered over

#### 3.6.0

* Added C# `AnimationOverride` and `CustomAnimations` classes for custom animation override support.

### Installation (manual)

For a manual installation, do the following

1. Create the folder(s): `Outward\BepInEx\plugins\SideLoader\`.
2. Extract the archive into a folder. **Do not extract into the Outward game folder.**
3. Move the contents of the `plugins\` folder from the archive into the `BepInEx\plugins\SideLoader` folder you created.
3. It should look like `Outward\BepInEx\plugins\SideLoader\SideLoader.dll`, etc
4. Launch the game, you should see a new keybinding in your in-game keybindings for the SideLoader menu.