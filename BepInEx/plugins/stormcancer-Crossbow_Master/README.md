______________________________________________________________

           ~Ever wanted to use Crossbows in Outward?~

 Then this mod is for you! This mod adds many Crossbows all over the world of Outward!

                 ~How can I find them?~

 Most Crossbows can be bought from Blacksmiths and Merchants. Some are craftable.   

              ~What special do they offer?~

 Crossbows are a mainhand onehanded range weapon, meaning you can combo it with any off hand weapon you want! Crossbows can only attack with the use Crossbow Shot skill and any other vanilla or custom bow skill but they can Zoom like any other normal bow. All Crossbows are enchantable, and in the future I will add Legacy versions for all of them too.
 

                   ~Warning!~

 Do not EVER use Crossbow Shot with normal Bows , its buggy...


           ~You want more Weapons too?
		   
 Also if you want new weapons and armors or new interesting effects aswell download "Weaponry and Enchantments Additions", "Armory Additions", "Knives Master" and "WeaponBalancePatch" too. I recommend them for a full weaponry experience. ;)


______________________________________________________________

                    -Future plans-
 Add more crossbows and Legacies!

                    +Communication+
 To report any bugs join Outward Modding Community on Discord and leave your commend on #stormcancer-workshop Channel! Thanks a lot!

                    #Contributors#
 Many thanks to iceboxX708,HMdesign,KaramellGlass,kyrylyushkov,deangothard for providing their amazing free crossbow models used in this mod via Sketchfab to the public and many thanks to all the Outward Modding Community for their help!


                       Enjoy! :)

______________________________________________________________

                    ~Spoilers Ahead!~

                       New Items:


                        Trainer:

Robin Lowun(Teaches you Crossbow Shot, can be found in Berg near the Hunter Trainer.)
                     

                         Skill:

Crossbow Shot(Required: Crossbow, Arrow. Shoot your targets using your equipped crossbow. (Warning! Don't use it with normal bows! Gamebreaking!))


                         Weapons:

Iron Crossbow(Sold by Blacksmiths.)

Brutal Crossbow(Sold by Blacksmiths.)

Obsidian Crossbow(Sold by Blacksmiths.)

Tsar Crossbow(Recipe sold by Blacksmiths.)

Wolf Crossbow(Sold by the Smuggler in Levant Slums.)

Palladium Crossbow(Sold by Blacksmiths.)

Galvanic Crossbow(Sold by Harmattans Blacksmith.)

Marble Crossbow(Sold by Harmattans Weaponsmith.)

Chalcedony Crossbow(Sold by New Siroccos Caravanner.)

______________________________________________________________