# Alchemist Skill Tree Mod by Schnabeldoktor

This mod adds a new skill tree: the Master Alchemist. Based around careful potion crafting, this tree is based around the core concept of an alchemical cycle. By sacrificing basic items, you will gain flexible skills that transform matter. With its breakthrough, your bombs will finally become useful tools in your arsenal! From beneficial to harmful, from specific to moldable, from residue to resource.

In order to begin your alchemical quest, speak to the visiting Alchemist in Cierzo. Follow his clues to find the trainer and a series of mysterious scrolls.

BIG THANKS TO: Nine Dots for making Outward, Gep for financing it. Sinai for SL, Emo for SL Extended Effects. StormCancer, IggyTheMad, Avrixel, Breadcrumb_Lord, Proboina and Emo for their endless patience and help. Fokson for the Active skill Icons. Hembrent, Kaipakta and EverBlack for their feedback and ideas. The Outward Modding Community for their support, ideas and amazing atmosphere.

## CAREFUL: DO NOT ADD MORE THAN ONE VERSION OF THIS MOD AT THE SAME TIME. IF YOU ADD THIS MOD IN ENGLISH, REMOVE THE SPANISH VERSION TO AVOID CLASHES!

## The Class has the following qualities:
* 5 new skills to transform matter under specific conditions, advancing from a basic first stage through 4 stages of alchemy, in order to achieve the reddening of matter - Iosis. These skills also deal some minor damage to enemies nearby if done properly.
* A mini Side-Quest: Noble Alchemists have arrived to Aurai and there's rumours that even a Magi of Alchemy has set up lab somewhere! Follow the clues to begin your training and to perform alchemical rituals!
* A highly flexible sigil to transmute matter, a must for any wandering alchemist! Not consuming a stone or mana, it might sound tempting at first. However, casting it requires having an alchemical artifact that requires constant maintenance.
* 3 very useful passives to introduce a new way of playing Outward: The bomb build! This tree's breakthrough removes the unpleasant delay that bombs have in order to make bombs a viable choice for a build. Alternatively, spam bombs to your heart's delight.
* A brand new improvement for the preservation backpack: the hermetic backpack! You will be carrying A LOT of potions, anyway! Find its recipe to craft it.
* A unique armor set tailored to the needs of all alchemists! If you find this skill's trainer, odds are you'll find the recipes for this!
* There is a price to transmutating matter! Skills are a double-edged sword: all skills within this tree require simple but specific conditions to be cast. Important: the skill tree won't prevent you from making mistakes! If these requirements are not met, VERY BAD THINGS WILL HAPPEN. Be prepared to experiment. Maybe you will find a use for the negative things that can happen!
* A fair skill tree: a high-risk, high-reward playstyle with skills that reward preparation and careful inventory management means that you will be punished for being careless but rewarded for planning ahead, without being as OP as laying 100 traps in front a manticore *ahem*

I made this mod 100% only with Sideloader and Sideloader Extended Effects for the plugin. Photopea for art. Unity for some prefabs. It took me 50 days of intermitent work, going from 0 knowledge of any of these programs to my end result.

I added all the skills / changes below if you want to spoil yourself and know the exact details of everything in the mod. I ADVICE AGAINST THIS FOR THE BEST EXPERIENCE!

This mod should not interfere with any other mods unless, idk, they remove bombs completely or place characters where I put them.

## What's new in the Alchemist Mod 1.1?

* **Alchemist 1.1: The Rites of Alchemy - Noble Alchemists have arrived to Aurai!**
![Visiting Alchemist](https://i.imgur.com/7TZPte6.png)
- Added a mini-clue-quest to learn about the rites - mysterious series of instructions to transmute matter.
  - Noble Alchemists can be found in cities. Varying in their profession, these masters of the craft will lead you to the locations of the Magi of Alchemy and mysterious scrolls.
  - Begin your quest in Cierzo. Keep your eyes open! You will find a clue in every city of the game that send you to seek the secret scrolls! Look around important locations and relevant areas!
  
  ![Letter to Alchemist](https://i.imgur.com/3KaueEO.png)
- Rebalanced: Lower weight for Bombs and Massa items. Added more flexibility to use of Massa items. Leucosis skill now an "ice skill". Small buff to "-osis" skills. Removed old sigil effects.
- Fixes: Massa items price down to 0, removed recipe-passive, changed massa item visuals. Added "doom" effect to Spark bomb. Fixed vendor location for Alchemist Robes.
- Expansion: Added recipe for Hermetic Backpack and location to buy it. Look around artificers, mages, alchemists and arcanists. Added new passive. Added new sigil effects. 



## A new character is recommended to enjoy this mod to its fullest.

# These are the skills without spoilers:

* `100 silver: Melanosis` First stage of alchemy, consumes food waste and produces massa nigra, a robust Hex cleaner that is full of corruption.

* `150 silver: Leucosis` Second stage of alchemy, consumes massa nigra and produces massa alba, a magical potion that turns buffs into debuffs and debuffs into buffs. It can also replace charges in recipes for bombs.

* `600 silver: Teachings of the Magi - Breakthrough` Hexes applied to you grant you a damage boost towards the opposite element. For example, being scorched boosts your ice damage by 5%. (Might be rebalanced in the future if more damage is needed). NOTICE: THIS PASSIVE WORKS PARTIALLY AS OF 20.SEPT.2022. Hexes you apply to yourself work properly. Hexes applied to you by enemies only trigger this passive if you reload the game. Working on a solution right now.

* Choice Skills:

  * `CHOICE A: 700 silver:  Experimental Concoction` Your bombs now explode almost instantly, but can damage ANYONE within their range. Damage and impact slightly nerfed (trust me, a needed balance change). 

  * `CHOICE B: 700 silver:  Alchemical Arsenal` You now throw 3 bombs that explode in rapid sucession. Damage and impact nerfed. Each bomb also builds the status effect by 35% instead of instantly afflicting with it. (trust me, a needed balance change). 

* `700 silver: Xanthosis` Third Stage of alchemy, consumes massa alba and produces massa flava, a flexible potion that can replace up to one ingredient in alchemy recipes, as long as:

1. the replaced ingredient is not ANY liquid (including potions, milk or liquifed corruption) or 

2. the replaced ingredient is not ANY form of mana stones, like a fire stone or crystal powder.

![Visiting Alchemist](https://i.imgur.com/L5Z2OPj.png) 

         An example: you can craft a life potion out of 1 massa flava + water + blood mushroom
![Visiting Alchemist](https://i.imgur.com/OHoica0.png)

         OR 1 massa flava + water + gravel beetle.
        
         Massa flava + Gravel beetle + Blood Mushroom will not work! 
         
![Visiting Alchemist](https://i.imgur.com/pYaBAV3.png)

         The massa flava can only replace liquids in recipes that are brewed out of 1 liquid + 1 ingredient.  For example, you can brew 1 firefly powder + 1 Massa flava to produce a blessed potion. This yields lower results though (in this case, instead of the usual 3 blessed potions, the output is 1 potion.

* `800 silver: Iosis` Fourth and final stage of alchemy, consumes massa flava and produces massa rubra, a powerful tonic that grants you a 20 second chemical rage that leaves you very vulnerable. It can also be used to restore the durability of the artificer's stone.

* `800 silver: Sigil of Alchemy` A sigil that requires having the artificer's stone in your inventory to be cast. The Combos will require experimentation or careful examination of clues spread throughout Aurai.

# Tips if you are stuck finding clues, finding the scrolls or understanding them:

* If you can't find clues: look around each main city. You will find them in important or relevant places.
* If you don't understand the clues, just go to the areas the clues tell you to go to and look thoroughly. The items won't be too far. 
* The scrolls are partially in English, Latin and Ancient Greek, but I chose words that are very similar to their English counterparts. Think a bit about similar words to understand them. They are pretty simple. Experiment and have fun!

# FAQ:

* Q: Why?

A : Because I love an alchemist archetype, I think bombs are very cool, and I need something to distract me from everyday life. I also hate coding but I love a challenge, so here we are. My "small bomb" mod became something bigger. Oh well. Neat.

* Q: What does this actually do?

A: This mod adds a skill tree with 5 active and 3 passive skills + new items and a load of new NPCS with a mini-side-clue-quest to unlock the powers of Alchemy. 

* Q: Are any other mechanics changed by this mod?

A: The only mechanic changed is how bombs work, other things shouldn't interfere with it. 

* Q: Which mods are needed for this mod to work?

A: Sideloader, Sideloader Extended Effects, maybe BepInEXPack and my other mod, the Alchemists_sell_bombs mod, which makes the bombs easier to craft and therefore not locked behind late-game content.

* Q: Where do I find the trainer?

A: Talk to the visiting alchemist in Cierzo to find a hint or read below for spoilers for his exact location.

* Q: Does this work online / on multiplayer?

A : Yes, but with some bugs here and there (check the "known bugs" category in this page). Both players should have this mod and all required mods (SL, SL Extended, BepInExPack, Alchemists_sell_bombs).

# CHANGELOG:

* `v1.0.0` - Release

* `v1.0.1` - README Fixes

* `v1.0.2` - Sigil of Alchemy HUD fix, Removing unintended perishing of items, fixing failed iosis delay, added alchemist armor, added recipes for alchemy set, added recipes to merchants, Typo Fixes, README fixes

* `v1.1.0` - Update 1.1 - The Rites of Alchemy - Added clues to perform rites. Sigil of Alchemy now has unique QOL spells instead of being a generic combat sigil. Added two different playstyles with bombs. Fixing droptables for alchemy armor. Rebalance. Fixes. Added Hermetic Backpack Recipe. Updated README. 		 

* `v1.1.1` - Fixed a bug with Passive "Teachings of the Magi". Removed unused passive for stone recipe.

* `v1.1.2` - README Fixes, fixed a typo in the Hermetic Backpack's description.

* `v1.1.3` - README Fixes, new rites, added extra pouch capacity for passive, fixed alchemist robe CD% reduction, removed durability from stones, added holy hand grenade for the lulz, buffed alchemical connection, fixed bug with traps, fixed wrong passive icon. 

* `v1.1.4` - Fixed wrong number in Alchemical connection.

* `v1.1.5` - Fixed bug with pouch passive.

* `v1.1.6` - Updated to latest SL Extended Effects. Bug which eats up all items when casting melanosis, leucosis, xanthosis or iosis should be gone. 

* `v1.1.7` - New mod icon, new sigil icon, bombs now have added effects when interacting with sigil. Full alchemist robe makes you immune to plague. 

* `v1.1.8` - Cleanse skill bug fix. 

* `v1.1.9` - Translation to Spanish + README Updates. 

# Known Bugs:

* The scenery items around the alchemists (bombs, alchemy kit) can be picked in multiplayer. This isn't intended but it's most likely a limitation of SL.

* Hex passive only seems to work with self-applied hexes or properly upon reload. Looking for a fix to this.

* Some skills seem to stop working after death - restarting the game fixes it.

* A user reported the active skills of this tree to consume more than one item when producing new ones. Should be fixed with latest SL extended effects.

## Other mods related to this one you might consider to fine-tune your experience:

[More bombs per recipe](https://outward.thunderstore.io/package/Schnabeldoktor/More_Bombs_Per_Recipe/) - A mod that slightly rises the yield of bombs crafted in the alchemy kit.

[Alchemist Mod - Spanish Translation](https://outward.thunderstore.io/package/Schnabeldoktor/Alchemist_Spanish/) - This mod translated into Spanish by ScarJaguar! <3 Gracias totales!

# SPOILERS BELOW - I EXTREMELY RECOMMEND YOU TO AVOID SPOILERS FOR THE BEST EXPERIENCE

* TRAINER LOCATION : Abrassar, north-west of the Nomad's Oasis, inside the small fortification with mana veins.

* Location for "rite scrolls":

    * Ancestor's resting place (in the room with the barrier armor bandit); in front of the gates of Sirocco, The Harmattan Libary on the upper floor; inside the trainer's tent; in the cabal outpost in Abrassar; in Turnbull's laboratory-

* Each skill can be cast with or without the required items. If they are cast without their required items, these will be the effects: 

    * `Failed Melanosis` Black Lung disease + mid-high decay damage in a cloud. No item created. 

    * `Failed Leucosis` Mercury Poisoning disease + mid-high decay damage in a cloud. No item created. 

    * `Failed Xanthosis` Arsenic Poisoning disease + high decay damage in a cloud. No item created. 

    * `Failed Iosis` Lead Poisoning disease + high fire damage in an explosion. No item created. 

    * All of these diseases can only be removed with panacea or death. 

* `Failed Sigil of Alchemy` Casting without artificer's stone produces a fire explosion and burns everyone. No sigil created.

*  `Recipe for Artificer's Stone` One tsar stone + Massa rubra + Dark stone + Leyline water in alchemy kit. If broken, repaired by combining the ruined stone + Massa rubra in an alchemy kit.

* Combinations with sigil:

    * `Sigil + Mana Ward` Sulphuric Brewery: Consume 1 salt to create 1 sulphur mushroom.

    * `Sigil + Mana Push` Leyline Tether: Fills an empty waterskin with 1 charge of leyline water.

    * `Sigil + Conjure` Pyro Fabrication: Conjures a lit fireplace.

    * `Sigil + Spark` Artificial Fumes: Consume 4 iron scraps to create 1 palladium scrap.

    * `Sigil + Cleanse` Magnum Opus: Consume 1 great life potion and 1 massa rubra to create 1 panacea.
	
    * `Sigil + Any boon + Egoth` Particle Synthesis: Consume a gold ingot and boon to create 1 elemental particle.
	
    * `Sigil + shim` Quartz Extraction: Consume a gold ingot and an illuminating potion to create 1 purifying quartz.

* Recipes for Alchemist Set (Alchemy Kit)

    * `Alchemist Robes` Entomber Armor + Noble Clothes + Petrified Wood + Grilled Marshmelon

    * `Alchemist Hat` Entomber Hat + Noble Mask + Hexa Stone + Miasmapod

    * `Alchemist Boots` Entomber Boots + Noble Shoes + Firefly Powder + Funnel Beetle

    * `Hermetic Backpack` Preservation Backpack + Shimmer Potion + Barrier Potion + Palladium Scrap

Purchase the recipes from Smooth, Levant's Tailor, or from Silver Tooth in Silkworm's Refuge, in Caldera. The Recipe for the backpack can be bought at the archanist in Harmattan.

# SPOILERS ABOVE - I EXTREMELY RECOMMEND YOU TO AVOID SPOILERS FOR THE BEST EXPERIENCE