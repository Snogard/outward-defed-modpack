# Cooking Master

______________________________________________________________

## Ever wanted to be a Chef in Outward?

 Well I got you covered. That's exactly what this mod does! You can now learn new cooking skills and recipes to create stronger foods with new powerfull effects, make better ingredients from simpler ones and share all that power with your coop team to make the most use out of your gourmet knowledge. You are the one who cooks! All Chef skills can be learned from various Chefs found in each town Inn.

## Ever wanted more cooking recipes?

 I got you there too! Some quality of life recipes are added too with new ways to create important foods and ingredients! Also added new drops to some enemies and traders for more availability of ingredients over regions.
______________________________________________________________

## Future plans
 More balance, even more custom cooking recipes.

## Communication
 To report any bugs join Outward Modding Community on Discord and leave your commend on stormcancer-workshop Channel! Thanks a lot!

## Contributors
 Many thanks to Emo for all his contribution with the new effects of Sideloader Extended and for his HUGE help on making and learning how to mod, Huge thanks to Sinai for the amazing Sideloader that gave me the ability to make that mod, thanks to Dapper Snake and Proboina for their helful feedback and thanks to all the Outward Modding Community for their help and ideas!

                       
### Enjoy! :)

______________________________________________________________


## Installation
- 1 Download [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)
- 2 Download [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)
- 3 Download [SideLoader ExtendedEffects](https://outward.thunderstore.io/package/SLExtendedEffects/SideLoader_ExtendedEffects/)
- 4 Unpack the contents of the zip file into the ...OutwardDefinitiveEd\BepInEx\plugins\stormcancer-Cooking_Master

______________________________________________________________

## Spoilers Ahead!

### Whats New:

#### Trainers:

- **Chef Allen**(Found in Cierzo Inn.)

- **Chef Bob**(Found in Berg Inn.)

- **Chef Rika**(Found in Monsoon Inn.)

- **Chef Charis**(Found in Levant Inn.)

- **Chef Jarel**(Found in Harmattan Inn.)

______________________________________________________________

### Master Chef:

![Chef](https://imgur.com/2O14d0e.png)

![Chef's Flattening Tools](https://imgur.com/zqYqrlN.png) - **Chef's Flattening Tools**(A rolling pin and a flattening board used by Chefs to mix Flour and any Egg into Dough.)

![Bakers' Secrets](https://imgur.com/XnBCSpA.png) - **Bakers' Secrets**(Gain new bakers' recipes.)

![Taste of a Chef](https://imgur.com/1Qx3pfv.png) - **Taste of a Chef**(Gain new Chef recipes, also grants immunity to Indigestion. Now consuming any chef's food instantly restores a moderate amount of your health, stamina or mana depending the food.)

![Chef's Cutting Tools](https://imgur.com/RSsCUSb.png) - **Chef's Cutting Tools**(A sharp knife and a cutting board used by Chefs to chop down Raw Salmon, Raw Meat, Alpha Raw Meat and Turmmip.)

![Chef's Grinding Tools](https://imgur.com/cU9Iuag.png) - **Chef's Grinding Tools**(A grinding stone and a grinding bowl used by Chefs to grind Seaweed, Common Mushroom, Krimp Nut and Crabeye Seed.)

![Chef's Preparation Tools](https://imgur.com/simRb31.png) - **Chef's Preparation Tools**(Required: Crabeye Oil. A big spoon and a pan used by Chefs to prepare sauses by Blood Mushroom, Sulphuric Mushroom, Nightmare Mushroom and Marshmelon.)

![Chef's Fermentation Tools](https://imgur.com/4VjhCdo.png) - **Chef's Fermentation Tools**(A big Barrel used by Chefs to ferment 4 Gaberry Jams, 4 Crawlberry Jams, 4 Cool Rainbow Jams and 4 Golden Jams into various liquers.)

![Cooks' Expertise](https://imgur.com/OkQcNrx.png) - **Cooks' Expertise**(Gain new Chersonese recipes, also grants immunity to Infection.)

![Gastronomy Secrets](https://imgur.com/fRLA27G.png) - **Gastronomy Secrets**(Gain new Enmerkar recipes, also grants immunity to Hive Infestation.)

![Essence Knowledge](https://imgur.com/fULrv5m.png) - **Essence Knowledge**(Gain new Hallowed Marsh recipes, also grants immunity to The Hunch.)

![Tasteful Insight](https://imgur.com/5iRL4bX.png) - **Tasteful Insight**(Gain new Abrassar cooking recipes, also grants immunity to Meeka Fever.)

![Gourmet Mastery](https://imgur.com/pSl0Wi3.png) - **Gourmet Mastery**(Gain new Antique Plateau recipes, also grants immunity to Leywilt.)

______________________________________________________________

#### Items:

- **Milk**(Renamed Boozu's Milk, now also chance to drops from Female Coralhorns and Boozus and Boreo. Food Merchants sell some too.)

- **Warm Milk**(Renamed Warm Boozu's Milk)

- **Krimp Nut**(Now also chance to drop from any Troglodyte)

- **Maize**(Now also chance to drop from any Phytosaur/Phytoflora)

- **Wheat**(Now also chance to drop from any Phytosaur/Phytoflora)

- **Sugar**(Now also drops from any Arcane Elemental Food Merchants sell some too.)

- **Bird Egg**(Now Food Merchants sell some too.)

- **Raw Monstrous Meat**(Drops from Shell Horrors and Manticores)

- **Cooked Monstrous Meat**(By cooking Raw Monstrous Meat)

- **Flour**(An added new recipe for flour to be made also from Maize, learned with Bakers' Secrets. Food Merchants sell some too.)

- **Dough**(Created with Chef's Flattening Tools and Flour and any Egg.)

- **Bread**(Recipe changed, can be learned with Bakers' Secrets)

- **Tortillas**(Recipe learned with Bakers' Secrets)

- **Spaghetti**(Recipe learned with Bakers' Secrets)

- **Spongecake**(Recipe learned with Bakers' Secrets)

- **Chopped Salmon**(Created with Chef's Cutting Tools and Raw Salmon.)

- **Chopped Meat**(Created with Chef's Cutting Tools and Raw Meat.)

- **Chopped Turmmip**(Created with Chef's Cutting Tools and Turmmip.)

- **Chopped Alpha Meat**(Created with Chef's Cutting Tools and Raw Alpha Meat.)

- **Mushroom Paste**(Created with Chef's Grinding Tools and Common Mushroom.)

- **Nori**(Created with Chef's Grinding Tools and Seaweed.)

- **Krimp Rise**(Created with Chef's Grinding Tools and Krimp Nut.)

- **Crabeye Oil**(Created with Chef's Grinding Tools and Crabeye Seed.)

- **Blood Sauce**(Created with Chef's Grinding Tools and Blood Mushroom.)

- **Sulphuric Sauce**(Created with Chef's Grinding Tools and Sulphuric Mushroom.)

- **Nightmare Sauce**(Created with Chef's Grinding Tools and Nightmare Mushroom.)

- **Marsh Sauce**(Created with Chef's Grinding Tools and Marshmelon.)

- **Gaberry Wine**(Created with Chef's Fermentation Tools and 4 Gaberry Jams.)

- **Crawlberry Whiskey**(Created with Chef's Fermentation Tools and 4 Crawlberry Jams.)

- **Cool Rainbow Gin**(Created with Chef's Fermentation Tools and 4 Cool Rainbow Jams.)

- **Golden Champagne**(Created with Chef's Fermentation Tools and 4 Golden Jams.)

- **Azure Pasta**(Recipe learned with Bakers' Secrets)

- **Shroojitas**(Recipe learned with Bakers' Secrets)

- **Green-Egg Toast**(Recipe learned with Bakers' Secrets)

- **Krimp Cake**(Recipe learned with Bakers' Secrets)

- **Pasticcio**(Recipe learned with Taste of a Chef)

- **Ocean's Tacos**(Recipe learned with Taste of a Chef)

- **Monstrous Sandwich**(Recipe learned with Taste of a Chef)

- **Beetles Cake**(Recipe learned with Taste of a Chef)

- **Souvlaki Stick**(Recipe learned with Cooks' Expertise)

- **Onigiri**(Recipe learned with Cooks' Expertise)

- **Salmon Sushi Roll**(Recipe learned with Cooks' Expertise)

- **Azure Tempura Roll**(Recipe learned with Cooks' Expertise)

- **Gaberry Cake**(Recipe learned with Cooks' Expertise)

- **Alpha Kodosouvli**(Recipe learned with Gastronomy Secrets)

- **Hunters' Fricassee**(Recipe learned with Gastronomy Secrets)

- **Alpha Explosive Burrito**(Recipe learned with Gastronomy Secrets)

- **Monstrous Bloody Pasta**(Recipe learned with Gastronomy Secrets)

- **Cool Rainbow Cake**(Recipe learned with Gastronomy Secrets)

- **Pandesia Salad**(Recipe learned with Essence Knowledge)

- **Soul Aura Salad**(Recipe learned with Essence Knowledge)

- **Special Paella**(Recipe learned with Essence Knowledge)

- **Fried Salmon a la Pesto**(Recipe learned with Essence Knowledge)

- **Elemental Fruit Salad**(Recipe learned with Essence Knowledge)

- **Jewel Shiny Nuggets**(Recipe learned with Tasteful Insight)

- **Corsairs' Tacos**(Recipe learned with Tasteful Insight)

- **The New Mystery**(Recipe learned with Tasteful Insight)

- **El Bistec y Nachos**(Recipe learned with Tasteful Insight)

- **Golden Cake**(Recipe learned with Tasteful Insight)

- **Green Sauced Boozu**(Recipe learned with Gourmet Mastery)

- **Harmattans' Ambrosia**(Recipe learned with Gourmet Mastery)

- **Salmon Teriyaki**(Recipe learned with Gourmet Mastery)

- **Carbonara**(Recipe learned with Gourmet Mastery)

- **Crawlberry Cake**(Recipe learned with Gourmet Mastery)


#### Legacies:

- **Legendary Pizza Chakram**(Legacy of Mysterious Chakram)

- **Legendary Cooks' Pan**(Legacy of Nori)

- **Legendary Cooks' Hat**(Legacy of Spaghetti)

- **Legendary Cooks' Garb**(Legacy of Krimp Rice)

- **Legendary Cooks' Boots**(Legacy of Crabeye Oil)

______________________________________________________________

#### Have fun Chefs!!!

![Chef](https://imgur.com/gQrv1F6.png)
______________________________________________________________
