## DLC Arrows outside of Caldera  Mod by Schnabeldoktor

This mod adds the arrows from TTB DLC to different locations throughout Aurai. You will find the arrows and their recipies in stashes, dead bodies, chests, etc. Arrowhead Kits will be sold by blacksmiths in somewhat low quantities.
In order to balance it, you will find that not all regions have all arrows and that their spawn rates will be a bit low. The strongest arrows will only be found in the hardest dungeons.

# The changes are such:
* All Blacksmiths sell arrowhead kits.
* Arrows and recipes can be found in stashes, dead bodies, chests etc.
* Arrows won't always drop and the amount of arrowhead kits in cities are low, so it seems balanced. Tell me on the Outward Modding discord if it feels too unbalanced/OP!
* Some enemy archers should also drop low level arrows.
* Specialized arrows can be found in chests inside Dungeons, the better the chest, the better the arrow type. Go into dungeons if you want to improve your odds!
* Specialized arrows spawn depending on area: for example, Explosive Arrows are more likely to spawn in Abrassar.
* Soul Rupture, Venom and Holy Rage arrows can only be found in Antique Plateau and Caldera Dungeons, for balance reasons. 
* Other mechanics related to arrows (Damage, Price, Weight, Recipes) haven't been changed.

I added ALL THE CHANGES below if you want to spoil yourself and know the exact location of all items.

This mod should not interfere with any other mods unless, idk, they remove sellers or arrows from the game or something like that. HOWEVER, this could probably collide with other mods that remove arrowhead kits from the game. Also, it could probably be unbalanced to have many mods that spawn arrows outside of Caldera at the same time. 
If you have some questions about this mod, you can ask me on the Outward modding discord.
However - I warn you - I did this with very robust knowledge of anything programming-related, so don't expect any thorough answers.

Thanks to Sinai for his Sideloader, Emo, StormCancer and IggyTheMadWhisperer for their patience and Raphendyr for his invaluable help with drop tables, which I now have learned to hate. Also thanks to the Outward Modding Community!

# ROADMAP

* Arrows, Arrowhead kits and recipes are sold by merchants (DONE)
* Re balance depending on feedback



# FAQ: 

* Q: Why?

A : Because waiting until the end of the Sirocco Questline for cool arrows is dumb. 

* Q: What does this actually do?

A: I added custom droptables to some droptables in the game. 

* Q: Are any other mechanics related to arrows changed by this mod?

A: No.


# CHANGELOG:

* `v1.0.0` - release

* `v1.0.1` - README fixes

* `v1.0.2` - Small rebalance: arrow spawn amount slightly lowered, especially for explosive arrows.

* `v1.0.3` - Added thanks.

* `v1.0.4` - README fixes.

-----------SPOILERS BELOW---------------------

I classified the arrows into the following groups:

* Low Tier: Flaming Arrow, Poison 
* Medium Tier: Forged Arrow
* High Tier: Explosive, Palladium, Mana
* OP Tier: Soul Rupture, Venom, Holy Rage
* Arrowheadkits: Blacksmiths

They spawn in these droptables, divided as such:

* Low Tier Arrows: DT_Neutral_Survival_Medium_1, DT_Chersonese_Equipment_High_1, DT_Chersonese_Equipment_HighDungeon_1, DT_HallowedMarsh_Equipment_Medium_1, DT_Abrassar_Equipment_Medium_1, DT_HallowedMarsh_Equipment_High_1, DT_Abrassar_Equipment_High_1, DT_Enmerkar_Equipment_Medium_1, DT_Enmerkar_Equipment_High_1, DT_BanditArcher_Enmercar_High, DT_BanditArcher_HallowedMarsh_High, DT_BanditArcher_Abrassar_Med, DT_BanditArcher_Kazite_Med, DT_AntiqueField_Equipment_Low_1
* Medium Tier Arrows: DT_Neutral_Survival_High_1, DT_HallowedMarsh_Equipment_High_1, DT_Abrassar_Equipment_High_1 , DT_Enmerkar_Equipment_High_1, DT_AntiqueField_Equipment_Medium_1, DT_Caldera_Equipment_Medium_1, DT_Caldera_Equipment_Low_1, DT_BanditArcher_KaziteLieutArcher_Antique_DLC1
* High Tier Arrows: DT_Neutral_Survival_High_1, DT_AntiqueField_Dungeon2B, DT_AntiqueField_Equipment_HighDungeon_1, DT_Caldera_Equipment_High_1
* OP Tier: DT_AntiqueField_Dungeon2B, DT_Caldera_Equipment_HighDungeon_1, DT_AntiqueField_Equipment_HighDungeon_1, DT_Caldera_ArenaRandomChest

* Explosive: DT_Abrassar_Equipment_HighDungeon_1
* Mana: DT_Enmerkar_Equipment_HighDungeon_1
* Palladium: DT_HallowedMarsh_Equipment_HighDungeon_1

Recipes should spawn here, with pretty low chances to spawn.

* Low Tier: DT_Abrassar_Recipe_Basic_1, DT_AntiqueField_Recipe_Basic_1, DT_Caldera_Recipe_Supply_1, DT_Enmerkar_Recipe_Weapon_1, DT_HallowedMarsh_Recipe_Weapon_1, DT_Neutral_StandardRecipe_Low_1
* High Tier: DT_Neutral_StandardRecipe_High_1
* OP Tier: DT_AntiqueField_Dungeon2B, DT_AntiqueField_Equipment_HighDungeon_1, DT_Caldera_Equipment_High_1
