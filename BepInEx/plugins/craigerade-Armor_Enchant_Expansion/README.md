# Armor_Enchant_Expansion

## Description
Many armors only have generic enchants. This mod adds a unique enchant to those armors.  
Enchants were chosen by the theme of the armor and not really for balance.

## Installation
- 1 Download and install [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)  
- 2 Download and install [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)  
- 3 Install with r2modman

## Changes
1. Ammolite set, Spirit of Cierzo chest, Formless helm and boots  
2. Blue Sand set, Spirit of Cierzo chest, Formless helm and boots  
3. Barrier set, Formless for whole set  
4. Mana wall, Formless for whole set  
5. Caged set, Light as wind, Primal wind, Guidance of wind  
6. Caldera Mail set, Cocoon whole set   
7. Chalcedony set, Warmaster whole set  
8. Crimson Plate set, Inner Cool whole set  
9. Dancer set, Economy whole set  
10. Entomber set, Chest Adrenaline, Helm Instinct, Boots Compass  
11. Gold Lich set, Chest Spirit of Monsoon, Helm Order and Discipline, Boots Unwavering determination  
12. Jade Lich set, Chest Spirit of Harmattan, Helm Arcane Uninson, Boots Speed and Efficiency  
13. Kintsugi and White Kintsugi set, Formless for whole set  
14. Krypteia set, Assassin whole set  
15. Master Trader set, Chest Unexpected Resilience, Helm Uassuming Ingenuity, Boots Uwavering Determination  
16. Militia and Vigilante sets, Rascal's Verve whole set  
17. Pearlescent Mail, Rascal's Verve  
18. Noble set, Economy whole set  
19. Petrified Wood set, Inner Warmth whole set    
20. Pilgrim Set, Warmaster whole set  
21. Rust Lich set, Chest Chaos and Creativity, Helm Order and Discipline, Boots Speed and Efficiency  
22. Scarlet set, Inner Warmth whole set  
23. Shock set, Chest Spirit of Monsoon, Helm Order and Discipline, Boots Elatt's Sanctity  
24. Slayer sest, Beast of Burden whole set  
25. Squire set, Chest Unexpected Resilience, Helm Uassuming Ingenuity, Boots Uwavering Determination  
26. Zagis set, Warmaster whole set  
27. Tsar set, Warmaster ,or unexpected unassuming unwavering ,or Formless ,or Chaos Order Speed

## Compatability
Confirmed working with Transmorphic enchating menu by ModifAmorphic.  
Currently tested working with Armory additions and weaponry and enchantments additions by stormcancer