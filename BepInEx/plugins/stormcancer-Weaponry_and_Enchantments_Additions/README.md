
# Weaponry and Enchantments Additions

______________________________________________________________

## Ever wanted new weapons?

 This mod aims to add new weapons and new enchantments! Also now all Legacy weapon sets (both new and vanilla) can be crafted by combining their non legacies and relics!

 Right now It offers many new enchantments, some vanilla removed weapons back to game and some new custom weapons!

 After Updated Version 1.2.3 this mod is fully compatible with "WeaponBalancePatch version:1.4" Mod of Proboina and I highly recommend it for more interesting weapons stats and effects!

 Also if you want the new daggers and armors aswell download "Knives Master" and "Armory Additions" too. I recommend them for a full weaponry experience. ;)

______________________________________________________________

## Future plans
 More weapons, more legacies, more enchantments and more enchantment related diseases!

## Communication
 To report any bugs join Outward Modding Community on Discord and leave your commend on #stormcancer-workshop Channel! Thanks a lot!

## Contributors
 Many thanks to Proboina for his contributions on Nightmare Set and many thanks to all the Outward Modding Community for their help!


### Enjoy! :)

______________________________________________________________


## Installation
- 1 Download [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)
- 2 Download [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)
- 3 Download [SideLoader ExtendedEffects](https://outward.thunderstore.io/package/SLExtendedEffects/SideLoader_ExtendedEffects/)
- 4 Unpack the contents of the zip file into the ...OutwardDefinitiveEd\BepInEx\plugins\stormcancer-Weaponry_and_Enchantments_Additions

______________________________________________________________

## Spoilers Ahead!

### Whats New:


#### Weapons:

- **Zagis' Brutal Hacksaw**(One Handed Sword. Recipe Sold by Levants Blacksmith.)

- **Giantkind Buster Sword**(Two Handed Sword. Sold by Giants Village Blacksmith.)

- **Obsidian Shield**(Recipe sold by Levants Blacksmith.)

- **Vampiric Shield**(Recipe sold in Harmattan university.)

- **Vampiric Chakram**(Recipe sold in Harmattan university.)

- **Vampiric Pistol**(Recipe sold in Harmattan university.)

- **Galvanic Rapier**(Recipe sold by Harmattan Blacksmith.)

- **Galvanic Claymore**(Recipe sold by Harmattan Blacksmith.)

- **Chalcedony Shield**(Recipe sold by Levants Blacksmith.)

- **Kazite Nodachi**(Can be dropped by Kazites.)

- **Marble Nodachi**(Sold by Harmattan Blacksmith.)

- **Galvanic Nodachi**(Recipe sold by Harmattan Blacksmith.)

- **Fire Mage’s Lexicon**(Found somewhere in Caldera.)

- **Frost Witch’s Lexicon**(Found somewhere in Antique Plateau.)

- **Necronomicon Lexicon**(Found somewhere in Chersonese.)

- **Voodoo Lexicon**(Found somewhere in Hallowed Marsh.)

- **Light Mender’s Lexicon**(Reworked Vanilla.)

- **Brutal Tower Shield**(Moved to Brutal Family.)

- **Brutal War Bow**(Moved to Brutal Family.)

- **Zagis' Brutal Saw**(Moved to Brutal Family.)

- **Brutal Cleaver**(Moved to Brutal Family.)

#### Legacies:

- **Nosferatus set**(Legacy evolutions for all Vampiric weapons or combined with Flowering Corruption.)

- **Meteoric Shield**(Legacy for Obsidian Shield or combined with Leyline Figment.)

- **Alpha set**(Legacy evolutions for all Wolf weapons or combined with Calixa's Relic.)

- **Nightmare set**(Legacy evolutions for all Horror weapons or combined with Scourge's Tears.)

- **Deepfreeze Shield**(Legacy evolution for Crimson Shield or combined with Leyline Figment.)

- **Sanguine Set**(Legacy for Brutal Set or combined with Vendavel's Hospitality.)


#### Vanilla Legacies:

- **Ancient Calygrey Set**(Legacy for Calygrey Set or combined with Calygrey's Wisdom.)

- **Masterpiece Set**(Legacy for Damascene Set or combined with Noble's Greed.)

- **Savage Set**(Legacy for Fang Set or combined with Haunted Memory.)

- **Gold/Golden Set**(Legacy for some low tier weapons or combined with Gep's Generosity.)

- **Ivory Master's Staff**(Legacy for Master's Staff or combined with Metalized Bones.)

- **Vigilante Set**(Legacy for Militia Set or combined with Calygrey's Wisdom.)

- **Meteoric Set**(Legacy for Obsidian Set or combined with Leyline Figment.)

- **Sinner Claymore**(Legacy for Prayer Claymore or combined with Vendavel's Hospitality.)

- **Cerulean Sabre**(Legacy for Steel Sabre or combined with Calixa's Relic.)

- **Legacies of Troglodyte Set**(Legacies of Troglodyte Set or combined with Flowering Corruption.)


#### Legacies recipes specific for Knives Master Mod:

- **Red Lady's Inferno Dagger/Knife**(Legacy for Red Lady's Dagger/Knife or combined with Scarlet Whisper.)

- **Zhorn’s Radiant Dagger/Knife**(Legacy for Zhorn’s Dagger/Knife or combined with Elatt's Relic.)



______________________________________________________________

#### Enchantments:

- **Contaminated**(100% Physical to Decay Damage, -100% Physical Damage. For Horror Weapons, their Legacies and Worldedge Greataxe. Sold in Harmattan university.)

- **Electrified**(125% Physical to Electric Damage, -100% Physical Damage. For Palladium Weapons and Zhorn's Weapons. Sold in Harmattan university.)

- **Etherealized**(160% Physical to Ethereal Damage, -100% Physical Damage. For Fang Weapons and their Legacies and Dreamer Halberd. Sold in Harmattan university.)

- **Frozen**(130% Physical to Frost Damage, -100% Physical Damage. For Chalcedony Weapons and Skycrown Mace. Sold in Harmattan university.)

- **Ignited**(130% Physical to Fire Damage, -100% Physical Damage. For Obsidian Weapons, their Legacies and Red Lady's Weapons. Sold in Harmattan university.)

- **Final Stage**(60% Physical to Decay Damage, 60% Physical to Ethereal Damage, -100% Physical Damage. For Vampiric Weapons and their Legacies. Sold in Harmattan university.)

- **Septic Shock**(Can inflict Extreme Bleeding and Extreme Poison. For Vampiric Weapons and their Legacies. Sold in Harmattan university.)

- **Wind of Luck**(+10% Movement Speed, 10% Chance to gain Speed Up or Unerring Read per hit. For Kazite Weapons. Sold in Harmattan university and can be droped by Kazites.)

- **Critical Superiority**(20% Chance to deal Superior Critical hit (60 Raw damage). For Tsar Weapons. Sold in Harmattan university.)

- **Critical Chance**(10% Chance to deal Normal Critical hit (30 Raw damage). For any weapon. Sold in Harmattan university.)

- **Golden Medicament**(+80% Electric Damage, 20% chance to heal you and remove all malevolant statuses per hit. For Gold Weapons. Sold in Harmattan university)

- **Marked Scent**(+30% Physical Damage, 50% Chance To apply a Marked stack (max 3) slowing the enemy, the third stack inflicts Extreme Bleeding. For Wolf Weapons and their Legacies. Sold in Harmattan university.)

- **Lunar Light**(30% Physical to Electric Damage, inflicts Burning and Holy Blaze. For Wolf Weapons and their Legacies. Sold in Harmattan university.)

- **Phoenix**(10% Fire Damage and 10% Stamina Cost Reduction. For Pearlbird Set and its Legacies. Sold in Harmattan university.)

- **Body Enhanter**(+0.05 Health Regeneration. For all armor parts. Sold in Harmattan university.)

- **Soul Enhancer**(+0.1 Mana Regeneration. For all armor parts. Sold in Harmattan university.)

- **Mind Enhancer**(5% Cooldown Reduction. For all armor parts. Sold in Harmattan university.)

- **Speed Enhancer**(5% Movement Speed. For all armor parts. Sold in Harmattan university.)

- **Physic Enhancer**(5% Stamina Cost Reduction. For all armor parts. Sold in Harmattan university.)

- **Mechanized**(+0.3 Attack Speed and 15% Stamina Cost Reduction. For Galvanic Weapons. Sold in Harmattan university.)

______________________________________________________________
