#Arrow Recipe Reworked - Weapon Balance Patch Module

###This is one of the modules from [Weapon Balance Patch mod](https://outward.thunderstore.io/package/Proboina/WeaponBalancePatch/), made as a separate file for your convenience

##DO NOT INSTALL BOTH

###Table of contents:
- **Arrowhead Kit (x3)**: now can be crafted with 3 Iron Spikes and Palladium Scrap in survival menu
- **Poison Arrow (x3)**: now can be also crafted with 3 Arrows and a Poison Rag in survival menu
- **Flaming Arrow (x3)**: now can be also crafted with 3 Arrows and a Fire Rag in survival menu
- **Forged Arrow (x3)**: now can be crafted with Iron Spike and Arrowhead Kit in survival menu
- **Palladium Arrow**: now crafted in survival menu instead of alchemy, no changes to the recipe

- No alchemy station recipes were changed
- That mod allowes for early game arrow crafting without breaking any balance of the game
