# Knives Master

______________________________________________________________

## Ever wanted to use daggers on your main hand?

 Well I got you covered. That's exactly what this mod does! You can craft every dagger on survival crafting with one thick oil and your got yourself a Knife for your main hand. To reverse any knife back to normal dagger just craft it with linen cloth!

## Ever wanted to enchant your daggers?

I got you there too! All daggers and knives are now able to be enchanted the same way any other weapon does!

## Ever wanted more daggers to choose from?

 Yeah that happened too! Now there are even more new daggers fitted into the vanilla weapon families! Some vanilla daggers got renamed and adjusted to fit those famillies and their style, some are removed vanilla daggers that I brought back to life, some are custom recolored daggers and some are brand new daggers with new custom models contributed by Gothiska, many thanks to him!!! (Also there are many new Legacy daggers and knives.)

## Ever wanted to play like an Assassin?

 Then Master Razon awaits you to meet him in your journeys to Aurai. Master Razon will teach you the tricks of Knives and the way of the Assassin, you will find him in Berg near the Assassin Trader.

## You want more weapons and armors?
	   
 After Updated Version 3.0.2 this mod is fully compatible with "WeaponBalancePatch version:1.4" Mod of Proboina and I highly recommend it for more interesting weapons stats and effects!

 Also if you want the new weapons and armors aswell download "Weaponry and Enchantments Additions" and "Armory Additions" too. I recommend them for a full weaponry experience. ;)
    
______________________________________________________________

## Future plans
 More balance, even more custom daggers.

## Communication
 To report any bugs join Outward Modding Community on Discord and leave your commend on stormcancer-workshop Channel! Thanks a lot!

## Contributors
 Again many thanks to Gothiska for the amazing 3d models. Many thanks to Proboina for his contributions on Nightmare Dagger and creation of Crescent Dagger! Also thanks a lot to Emo for his HUGE help on making and learning how to mod, and thanks to all the Outward Modding Community for there help and ideas!

                       
### Enjoy! :)

______________________________________________________________


## Installation
- 1 Download [BepInEx](https://outward.thunderstore.io/package/BepInEx/BepInExPack_Outward/)
- 2 Download [SideLoader](https://outward.thunderstore.io/package/sinai-dev/SideLoader/)
- 3 Download [SideLoader ExtendedEffects](https://outward.thunderstore.io/package/SLExtendedEffects/SideLoader_ExtendedEffects/)
- 4 Unpack the contents of the zip file into the ...OutwardDefinitiveEd\BepInEx\plugins\stormcancer-Knives_Master

______________________________________________________________

## Spoilers Ahead!

### Whats New:


#### Weapons:

- **Junk Dagger**(Overhaul of Shiv Dagger.)

- **Iron Dagger**(Overhaul of Rondel Dagger)

- **Brutal Dagger**(Overhaul of Broad Dagger)

- **Horror Dagger**(Recipe sold by Monsoons Alchemist.)

- **Obsidian Dagger**(Recipe sold by Bergs Assassin Merchant.) (Obtained from Kazite Spellblade)

- **Chalcedony Dagger**(Recipe sold by Cierzos Alchemist Merchant.)

- **Damascine Dagger**(Recipe sold by Bergs Alchemist Merchant.)

- **Kazite Dagger**(Can be dropped by Kazites.)

- **Fang Dagger**(Recipe sold by Bergs Assassin Merchant.)

- **Marble Dagger**(Sold by Harmattans Blacksmith.)

- **Virgin Dagger**(Sold by Harmattans Blacksmith.)

- **Palladium Dagger**(Sold by Monsoons Blacksmith.)

- **Zhorn's Glowstone Dagger**(Got Buffed.)

- **Tsar Dagger**(Recipe sold by Monsoons Blacksmith.)

- **Crescent Dagger**(Recipe sold by Cierzos Alchemist Merchant)


#### Legacies:

- **Masterpiece Dagger**(Legacy evolution of Damascine Dagger.)

- **Meteoric Dagger**(Legacy evolution of Obsidian Dagger.)

- **Golden Junk Dagger**(Legacy evolution of Junk Dagger.)

- **Red Lady's Inferno Dagger**(Legacy evolution of Red Lady's Dagger.)

- **Zhorn's Radiant Dagger**(Legacy evolution of Zhorn's Glowstone Dagger.)

- **Savage Dagger**(Legacy evolution of Fang Dagger.)

- **Nosferatus Dagger**(Legacy evolution of Vampiric Dagger.)

- **Alpha Dagger**(Legacy evolution of Wolf Dagger.)

- **Nightmare Dagger**(Legacy evolution of Horror Dagger.)

- **Sanguine Dagger**(Legacy evolution of Brutal Dagger.)

______________________________________________________________

### Shadow Assassin:

![Assassin](https://imgur.com/AJ0hAkv.png)

![Dual Slash](https://imgur.com/H7rOcqL.png) - **Dual Slash**(Required: Knife and Dagger. A dual slash attack that builds up Bleed Out on bleeding enemies. Successful attacks on bleeding enemies also refund most of its cooldown.)

![Countertrick](https://imgur.com/bpfuOZx.png) - **Countertrick**(Blocks an attack and blinks back.)

![Assassin's Mark](https://imgur.com/OEVRSST.png) - **Assassin's Mark**(Randomly marks enemies around you with the Assassin's Mark, attacking a marked enemy inflicts them with Bleeding and boost your movements speed for a short duration consuming the mark.)

![Dance of Dagger](https://imgur.com/T0hjm9C.png) - **Dance of Daggers**(Required: Knife and Dagger. Two instant slash attacks that hit all enemies around you inflicting them with Pain followed by a round kick at the end that inflicts Confusion.)

![Blink Strike](https://imgur.com/uL8cjXb.png) - **Blink Strike**(Rushing round slash that blinks through the enemies and steals their Mana.)

![Assassination](https://imgur.com/6SgLYlU.png) - **Assassination**(Required: Knife and Dagger. Leap and slash your enemy. Enemies with Bleeding or Extreme Bleeding take extra damage and impact. More effective if the blow is dealt on the back of an enemy.)

![Killer Instinct](https://imgur.com/HEVtIBM.png) - **Killer Instinct**(Grants Instict stacks over time, each stack decrease your Stamina cost by 5% and increase the Physical damage you inflict by 5%, attacking an enemy with your weapons consumes all stacks.)

![Shadow Cloak](https://imgur.com/d1rWA5X.png) - **Shadow Cloak**(Cloak yourself with darkness and vanish for a short duration. Any attack in that duration breaks the effect but deals more Physical Damage.)

______________________________________________________________

## Trainer Location

![trainer](https://i.imgur.com/yn6wiYt.png)

______________________________________________________________